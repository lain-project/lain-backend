# Lain backend

## Montaje del entorno de desarrollo

1. En caso de no tener instalado debe [instalar docker](https://docs.docker.com/get-docker/) y [docker-compose](https://docs.docker.com/compose/install/).
2. Ejecutar `docker-compose up` para levantar la aplicación (La primera vez va a realizar el build de las imagenes de los servicios especificados en el archivo docker-compose.yml por lo que puede llegar a tardar, luego tarda unos segundos nada más).
3. Para bajar el container creado, se debe utilizar el comando `docker-compose down`

## Validación

Para validar que el servidor esta arriba se debe acceder a la direccion [http://localhost:8000/docs](http://localhost:8000/docs).