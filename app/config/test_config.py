from typing import Any, Dict, List, Optional, Union
from pydantic import AnyHttpUrl, BaseSettings, PostgresDsn, validator


class TestConfig(BaseSettings):

    TESTING: bool = False
    PROJECT_NAME: str = "Lain Testing"
    SERVER_NAME: str = "Lain Testing - API"
    ENVIROMENT: str = "TESTING"
    ROUTE: str = "/api/v1/testing"
    SECRET_KEY: str = "secret"
    # 60 minutes * 24 hours * 7 days = 1 week = 7 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 7

    # SERVER_NAME: str = "Lain Development - API"
    SERVER_HOST: AnyHttpUrl = "http://localhost:8000"

    CORS_ORIGINS: List[AnyHttpUrl] = ["http://localhost:3000"]

    # Database
    POSTGRES_SERVER: str = "localhost"
    POSTGRES_USER: str = "postgres"
    POSTGRES_PASSWORD: str = "postgres"
    POSTGRES_DB: str = "lain-test"
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["): # pragma: no cover
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v) # pragma: no cover


    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str): # pragma: no cover
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}"
        )


    # Neuralactions

    NEURALACTIONS_SERVER: str = "https://app.neuralactions.com/api/v1"
    URL_LOGIN: str = f'{NEURALACTIONS_SERVER}/login'
    USER_NA: str
    PASS_NA: str

    # Google
    GOOGLE_CLIENT_ID: str
    GOOGLE_SECRET_ID: str

    class Config:
        env_file = ".env"
        case_sensitive = True

test = TestConfig()
