from typing import Any, Dict, List, Optional, Union
from pydantic import AnyHttpUrl, BaseSettings, PostgresDsn, validator


class DevConfig(BaseSettings):

    TESTING: bool = False
    PROJECT_NAME: str = "Lain Development"
    SERVER_NAME: str = "Lain Development - API"
    ENVIROMENT: str = "DEVELOPMENT"
    ROUTE: str = "/api/v1/developer"
    SECRET_KEY: str = "secret"
    # 60 minutes * 24 hours * 7 days = 1 week = 7 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 7

    # SERVER_NAME: str = "Lain Development - API"
    SERVER_HOST: AnyHttpUrl = "http://localhost:8000"

    CORS_ORIGINS: List[AnyHttpUrl] = ["http://localhost:3000"]

    # Database
    POSTGRES_SERVER: str = "wired-database"
    POSTGRES_USER: str = "postgres"
    POSTGRES_PASSWORD: str = "postgres"
    POSTGRES_DB: str = "lain"
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["): # pragma: no cover
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v) # pragma: no cover


    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str): # pragma: no cover
            return v
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}"
        )


    # Neuralactions

    NEURALACTIONS_SERVER: str = "https://demo.neuralactions.com/api/v1"
    URL_LOGIN: str = f'{NEURALACTIONS_SERVER}/login'
    URL_WORKSPACE: str = f'{NEURALACTIONS_SERVER}/workspaces'
    URL_SCHEMA: str = f'{NEURALACTIONS_SERVER}/nodes/scheme'
    URL_NODE: str = f'{NEURALACTIONS_SERVER}/nodes'
    URL_RELATIONSHIP: str = f'{NEURALACTIONS_SERVER}/edge'
    USER_NA: str
    PASS_NA: str

    # Google
    GOOGLE_CLIENT_ID: str
    GOOGLE_SECRET_ID: str

    class Config:
        env_file = ".env"
        case_sensitive = True

dev = DevConfig()
