import logging
from app.application.exception.bad_request_business_exception import BadRequestBusinessErrorException
from app.application.exception.conflict_business_exception import ConflictBusinessException
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
from app.application.exception.unauthorized_error_exception import UnauthorizedErrorException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse


def handler_error(app): # pragma: no cover 

    @app.exception_handler(BadRequestBusinessErrorException)
    async def handle_business_error_exception(request, exc):
        logging.error('handle_business_error_exception: NotFoundBusinessErrorException')
        return JSONResponse(status_code=400, content=jsonable_encoder(exc))

    @app.exception_handler(UnauthorizedErrorException)
    async def handle_business_error_exception(request, exc):
        logging.error('handle_business_error_exception: UnauthorizedErrorException')
        return JSONResponse(status_code=401, content=jsonable_encoder(exc))

    @app.exception_handler(NotFoundBusinessErrorException)
    async def handle_business_error_exception(request, exc):
        logging.error('handle_business_error_exception: NotFoundBusinessErrorException')
        return JSONResponse(status_code=404, content=jsonable_encoder(exc))

    @app.exception_handler(InternalBusinessErrorException)
    async def handle_business_error_exception(request, exc):
        logging.error('handle_business_error_exception: InternalBusinessErrorException')
        return JSONResponse(status_code=500, content=jsonable_encoder(exc))
    
    @app.exception_handler(ConflictBusinessException)
    async def handle_business_error_exception(request, exc):
        logging.error('handle_business_error_exception: ConflictBusinessException')
        return JSONResponse(status_code=409, content=jsonable_encoder(exc))
    

