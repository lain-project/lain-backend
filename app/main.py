import uvicorn

from app.config.custom_logging import setup_logging
from app.config.handler_error import handler_error
from app.config.config import settings
from app.adapter.controller.api import api_router
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware


def run():

    app = FastAPI(title=settings.SERVER_NAME, openapi_url=settings.ROUTE)
    setup_logging()
    app.include_router(api_router, prefix=settings.ROUTE)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    handler_error(app)
    return app

def main():
    uvicorn.run(app="app.main:run", host="0.0.0.0", port=8000, reload=True, workers=2)

app = run()
