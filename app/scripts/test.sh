#!/bin/bash
# docker exec lain-backend poetry run coverage run -m pytest  -v -m "unit" app/tests/*
# docker exec lain-backend poetry run coverage run -m pytest  -v -m "integration" app/tests/*
#docker exec -i lain-backend poetry run pytest --asyncio-mode=strict --html=report.html app/tests/*
docker exec -i lain-backend rm -rf report.html
docker exec -i lain-backend poetry run coverage run -m pytest --html=report.html --disable-pytest-warnings --asyncio-mode=strict -v app/tests/*
docker exec -i lain-backend poetry run coverage report -m
docker exec -i lain-backend poetry run coverage html --omit="*/tests/*"
brave-browser htmlcov/index.html
brave-browser report.html