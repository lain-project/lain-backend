from dataclasses import dataclass
from typing import Optional


@dataclass
class Profile:
    lastname: str
    firstname: str
    position: Optional[str] = None