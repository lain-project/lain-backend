from dataclasses import dataclass

@dataclass
class Schema:
    id: int | None
    name: str | None
    description: str | None
    schema_id: str | None
    study_plan_id: int | None

