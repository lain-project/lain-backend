
from enum import Enum

class RolNameEnum(Enum):
    ADMIN = "ADMIN"
    DOCENTE = "DOCENTE"