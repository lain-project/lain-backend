from enum import Enum
from typing import Literal

class ProfilePositionEnum(Enum):
    TITULAR: str = 'Titular'
    ADJUNTO: str = 'Adjunto'

