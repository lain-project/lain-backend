from enum import Enum

class UserStatusEnum(Enum):
    ACTIVE = 1
    INACTIVE = 2