import logging

from app.domain.value_objects.regimen import Regimen

from app.domain.value_objects.codigo import Codigo

from app.domain.value_objects.nombre import Nombre

from app.domain.entities.materia import Materia


class MateriaFactory:

    @staticmethod
    def crear_materia_base(nombre: str, codigo: str, regimen: str) -> Materia:
        logging.info("Creando materia base" + nombre + " " + codigo + " " + regimen)
        return Materia(
            Materia.next_id(),
            Nombre(nombre),
            Codigo(codigo),
            Regimen(regimen)
        )

    @staticmethod
    def crear_materia_completa(identificador: str, nombre: str, codigo: str, regimen: str, horas_semanales: int,
                               horas_totales: int) -> Materia:
        materia = Materia(identificador, Nombre(nombre), Codigo(codigo), Regimen(regimen.upper()))
        materia.horas_semanales = horas_semanales
        materia.horas_totales = horas_totales
        return materia
