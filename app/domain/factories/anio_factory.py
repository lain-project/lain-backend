from app.domain.study_plan import StudyPlan

from app.domain.entities.anio import Anio


class AnioFactory:

    @staticmethod
    def crear_anio(identificador: int, numero: int, nombre: str, plan_de_estudio_id: int) -> Anio:
        anio = Anio(identificador, numero, nombre)
        anio.plan_de_estudio = StudyPlan(plan_de_estudio_id)
        return anio

    @staticmethod
    def crear_anio_sin_plan_de_estudio(identificador: int, numero: int, nombre: str) -> Anio:
        return Anio(identificador, numero, nombre)

    @staticmethod
    def crear_anio_con_plan_de_estudio(identificador: int, numero: int, nombre: str, plan_de_estudio: StudyPlan) -> Anio:
        return Anio(identificador, numero, nombre, plan_de_estudio)