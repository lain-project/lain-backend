from app.domain.study_plan import StudyPlan


class PlanDeEstudioFactory:

    @staticmethod
    def crear_plan_de_estudio(identificador: int, codigo: str, nombre: str, description: str) -> StudyPlan:
        return StudyPlan(identificador, codigo, nombre, description)
