from dataclasses import dataclass


@dataclass
class UserToken:
    username: str
    email: str
    avatar: str
    rolname: str
    google: bool
    id: int = None
