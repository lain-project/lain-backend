from dataclasses import dataclass, field

@dataclass
class Year:
    id: int = None
    name: str = None
    numeration: int = None
    schema_id: str = None
    workspace_id: str = None
    study_plan_id: str = None
    node_id: str = None
