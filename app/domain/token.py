from dataclasses import dataclass

@dataclass
class Token:
    access_token: str = None
    refresh_token: str = None
    expire: int = None
    token_neuralaction: str = None
