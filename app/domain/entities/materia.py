from dataclasses import dataclass, field, asdict

from app.domain.value_objects.regimen import Regimen
from app.domain.value_objects.nombre import Nombre
from app.domain.value_objects.codigo import Codigo
from app.shared_kernel.domain.entities import AggregateRoot
from app.domain.entities.anio import Anio


@dataclass
class Materia(AggregateRoot):
    nombre: Nombre
    codigo: Codigo
    regimen: Regimen
    anio: Anio | None = field(init=False, repr=False)
    horas_semanales: int = field(init=False, default=0)
    horas_totales: int = field(init=False, default=0)

    def establecer_anio(self, anio: Anio):
        self.anio = anio

    def obtener_nombre(self):
        return self.nombre.nombre

    def obtener_codigo(self):
        return self.codigo.codigo

    def obtener_regimen(self):
        return self.regimen.name

    def obtener_plan_de_estudio(self):
        if self.anio is not None:
            return self.anio.plan_de_estudio

    @property
    def id_plan_de_estudio(self):
        return self.anio.plan_de_estudio.id