from dataclasses import dataclass, field

from app.domain.study_plan import StudyPlan

from app.shared_kernel.domain.entities import Entity


@dataclass
class Anio(Entity):
    numero: int
    nombre: str
    plan_de_estudio: StudyPlan | None = field(default=None)



