from app.domain.value_objects.nombre import Nombre


class Bibliography:
    name: Nombre | None
    isbn: str | None

