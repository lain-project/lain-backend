class NombreException(Exception):
    def __init__(self, mensaje: str):
        self.mensaje = mensaje

    def __str__(self):
        return self.mensaje