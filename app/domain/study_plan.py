from dataclasses import dataclass

@dataclass
class StudyPlan:
    id: int = None
    code: str = None
    name: str = None
    description: str = None
    years: int = None
    workspace_id: str = None