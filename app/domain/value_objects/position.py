from enum import Enum


class Position(Enum):

    TITULAR = 'Titular'
    ADJUNTO = 'Adjunto'