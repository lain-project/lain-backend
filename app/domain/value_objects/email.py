from app.domain.exceptions.email_exception import EmailException


class Email:

    def __init__(self, user: str, domain: str):

        if user is None or user is '':
            raise EmailException('Invalid email, not have username')

        if domain is None or domain is '':
            raise EmailException('Invalid email, not have domain')

        self._user = user
        self._domain = domain

    @classmethod
    def from_text(cls, address: str):
        if '@' not in address:
            raise EmailException("Email address must contain '@'")
        user, domain = address.split('@')
        return cls(user, domain)

    @property
    def email(self):
        return f'{self._user}@{self._domain}'


    def __str__(self):
        return f'{self._user}@{self._domain}'