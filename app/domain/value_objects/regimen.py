from dataclasses import dataclass
from app.domain.exceptions.regime_exception import RegimeException


@dataclass(init=False)
class Regimen:
    nombre: str

    def __init__(self, nombre):
        scopes = ['ANUAL', 'CUATRIMESTRAL']
        if nombre is None or nombre not in scopes:
            raise RegimeException('Invalid regime')
        self.nombre = nombre

    @property
    def name(self):
        return self.nombre.capitalize()
