from app.domain.exceptions.nombre_exception import NombreException


class Nombre:
    _nombre: str

    def __init__(self, nombre):
        if nombre is None or nombre is '':
            raise NombreException('El nombre no debe estar vacio')
        self._nombre = nombre

    @property
    def nombre(self):
        return self._nombre.capitalize()

    def __eq__(self, other):
        return self._nombre == other.nombre