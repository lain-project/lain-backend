from app.domain.repositorios.identificador_repository import IdentificadorRepositorio


class Identificador:

    def __init__(self, idd: IdentificadorRepositorio, idd_str=None):
        if not idd_str is None:
            self._id = idd_str
        else:
            self._id = idd.crear_nuevo_id()

    @property
    def id(self):
        return self._id

    def __eq__(self, other):
        return str(self) == str(other)

    def __repr__(self):
        return f"{self.id}"

    def __str__(self):
        return f"{self.id}"