import re
from app.domain.exceptions.codigo_exception import CodigoException


class Codigo:

    def __init__(self, valor: str):

        if valor is None or valor is '':
            raise CodigoException('El codigo debe tener algun caracter')

        if valor.isdigit() is False:
            raise CodigoException('El código solo debe contener numeros')

        self._codigo = valor

    @property
    def codigo(self):
        return self._codigo

    def __eq__(self, other):
        return self._codigo == other.codigo
