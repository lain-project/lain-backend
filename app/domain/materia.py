from dataclasses import dataclass
from typing import List

from app.domain.value_objects.regimen import Regimen
from app.domain.value_objects.nombre import Nombre
from app.domain.value_objects.anio import Anio
from app.domain.value_objects.identificador import Identificador
from app.domain.value_objects.codigo import Codigo


@dataclass
class Materia:
    id: Identificador | None
    nombre: Nombre | None
    codigo: Codigo | None
    regimen: Regimen | None
    horas_semanales: int | None
    horas_totales: int | None
    anio: Anio | None
    units: List[Identificador] | None

    def __init__(self, idd: Identificador, nombre: Nombre, codigo: Codigo, regimen: Regimen):
        self._idd = idd
        self._nombre = nombre
        self._codigo = codigo
        self._regimen = regimen
        self._unidades = []
        # self._

    #
    # def establecer_anio(self, anio: Anio):
    #     self._anio = anio
    #
    # def establecer_horas_semanales(self, horas):
    #     self._horas_semanales = horas
    #
    # def agregar_unidad(self, unidad: Unidad):
    #     self._unidades.append(unidad)
    #
    # def actualizar_unidad(self, unidad: Unidad):
    #     self.eliminar_unidad(unidad.id)
    #     self.agregar_unidad(unidad)
    #
    # def eliminar_unidad(self, unidad_id: Identificador):
    #     self._unidades = [u for u in self._unidades if u.id != unidad_id]
    #
