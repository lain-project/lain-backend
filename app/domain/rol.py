from dataclasses import dataclass


@dataclass
class Rol:
    id: int = None
    name: str = None
    description: str = None

