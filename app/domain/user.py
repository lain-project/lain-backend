from dataclasses import dataclass
from typing import Optional
from app.domain.profile import Profile

from app.domain.rol import Rol


@dataclass
class User:
    username: str 
    email: str 
    password: str 
    id: Optional[int]= None
    avatar: Optional[str] = None
    google: Optional[bool] = None
    status: Optional[int] = None
    rol: Optional[Rol] = None
    profile: Optional[Profile] = None
    
    def __eq__(self, other):
        id = self.id == other.id
        username = self.username == other.username
        email = self.email == other.email

        return (id or username or email) 