from dataclasses import dataclass
from datetime import datetime

@dataclass
class TokenNeuralAction:

    access_token:str = None
    token_type: str = None
    user_id: int = None
    expires_at: datetime = None