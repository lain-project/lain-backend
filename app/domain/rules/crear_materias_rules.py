from dataclasses import dataclass
from typing import List

from app.domain.value_objects.nombre import Nombre

from app.domain.value_objects.codigo import Codigo

from app.domain.entities.materia import Materia

from app.shared_kernel.domain.rules import BusinessRule
from app.domain.exceptions.regla_de_negocio_exception import ReglaDeNegocioException


@dataclass
class NoDebeExistirDosMateriasConElMismoCodigo(BusinessRule):
    materias: List[Materia]
    codigo: Codigo

    def is_broken(self) -> bool:
        if len(self.materias) == 0:
            return False
        if self.codigo in [materia.codigo for materia in self.materias]:
            raise ReglaDeNegocioException(self.get_message())
        return False

    def get_message(self) -> str:
        return f"Ya existe una materia con el codigo {self.codigo.codigo}"


@dataclass
class NoDebeExistirDosMateriasConElMismoNombre(BusinessRule):
    materias: List[Materia]
    nombre: Nombre

    def is_broken(self) -> bool:
        if len(self.materias) == 0:
            return False
        if self.nombre in [materia.nombre for materia in self.materias]:
            raise ReglaDeNegocioException(self.get_message())
        return False

    def get_message(self) -> str:
        return f"Ya existe una materia con el nombre {self.nombre.nombre}"