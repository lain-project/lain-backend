import logging
from app.application.port.output.neuralactions_repository import NeuralactionsRepository
from app.application.port.output.token_repository import TokenRepository
from app.domain.study_plan import StudyPlan
from app.domain.year import Year


class NeuralactionDomainService:

    def __init__(self, token_repository: TokenRepository, 
                        neuralactions_repository: NeuralactionsRepository):
        self.token_repository = token_repository
        self.neuralactions_repository = neuralactions_repository



    async def get_token(self) -> str:
        logging.info(f'Se procede a buscar token en la base de datos')
        token_neuralaction = await self.token_repository.get_token()
        if token_neuralaction is None:
            return await self._authenticate_in_neuralactions()
        if await self.token_repository.is_expired(token_neuralaction.access_token):
            return await self._authenticate_in_neuralactions()
        logging.info('Token obtenido exitosamente')
        return token_neuralaction.access_token

    
    async def _authenticate_in_neuralactions(self):
        logging.info(f'Se procede a logear en neuralactions')
        token_neuralaction = await self.neuralactions_repository.authenticate()
        await self.token_repository.save_token(token_neuralaction)
        logging.info(f'Autenticado en neuralactions')
        return token_neuralaction.access_token
    

    async def create_workspace(self,study_plan: StudyPlan) -> str:
        logging.info(f'Se procede a plan de estudio como workspace')
        self.neuralactions_repository.set_headers(await self.get_token())
        workspace_name = f'{study_plan.code}-{study_plan.name}'
        workspace_id = await self.neuralactions_repository.save_workspace(workspace_name, study_plan.description)
        logging.info(f'Se creo workspace exitosamente')
        return workspace_id

    async def create_schemas(self, workspace_id: str):
        self.neuralactions_repository.set_headers(await self.get_token())
        logging.info(f'Se procede a crear schemas en workspace')
        schemas = await self.neuralactions_repository.create_schemas(workspace_id)
        logging.info(f'Se crearon schemas exitosamente')
        return schemas

    async def update_workspace(self, study_plan: StudyPlan, workspace_id: str):
        logging.info(f'Se procede a actualizar workspace')
        self.neuralactions_repository.set_headers(await self.get_token())
        workspace_name = f'{study_plan.code}-{study_plan.name}'
        result = await self.neuralactions_repository.update_workspace(workspace_id, workspace_name, study_plan.description)
        logging.info(f'Se creo workspace exitosamente')
        return result

    async def create_years(self, workspace_id: str, years: Year):
        logging.info(f'Se procede a crear años en workspace')
        self.neuralactions_repository.set_headers(await self.get_token())
        result = await self.neuralactions_repository.save_years(workspace_id, years)
        logging.info(f'Se creo materia exitosamente')
        return result