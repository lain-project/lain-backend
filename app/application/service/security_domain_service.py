import logging
from datetime import datetime, timedelta
from app.application.exception.unauthorized_error_exception import UnauthorizedErrorException
from app.application.port.output.security_repository import SecurityRepository
from app.domain.token import Token
from app.domain.user import User
from app.domain.user_token import UserToken


class SecurityDomainService:

    def __init__(self, security_repository: SecurityRepository):
        self.security_repository = security_repository
    
    def expire_date(self, days: int) -> int:
        date = datetime.now()
        new_date =  date + timedelta(days=days)
        return new_date

    def write_token(self, data: User) -> Token:
        logging.info(f'Generarion del token para el usuario {data.username}')
        user_token = UserToken(data.username, data.email, data.avatar, data.rol.name, data.google, data.id)
        token = self.security_repository.encode(user_token, self.expire_date(2))
        logging.info(f'Token generado exitosamente')
        return Token(access_token=token)

    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        logging.info(f'Se procede a verificar password')
        is_valid = self.security_repository.verify_password(plain_password, hashed_password)
        if not is_valid:
            logging.error(f'Error al validar el password')
            raise UnauthorizedErrorException(f'Error al validar el password')
        return is_valid

    def get_password_hash(self, password: str) -> str:
        return self.security_repository.get_hashed_password(password)

    # def validate_token(self, token: str) -> UserToken:
    #     return self.security_repository.decode(token)
