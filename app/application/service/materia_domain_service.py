import logging

from app.application.port.output.autenticacion_neuralactions_repository import AutenticacionNeuralactionsRepository
from app.application.port.output.token_repository import TokenRepository
from app.domain.entities.materia import Materia
from app.application.port.output.materia_neuralactions_repository import MateriaNeuralactionsRepository


class MateriaDomainService:

    def __init__(self, token_repository: TokenRepository,
                 autenticacion_neural_repo: AutenticacionNeuralactionsRepository,
                 materia_neural_repo: MateriaNeuralactionsRepository):
        self.token_repository = token_repository
        self.autenticacion_neural_repo = autenticacion_neural_repo
        self.materia_neural_repo = materia_neural_repo

    async def guardar_materia(self, materia: Materia) -> Materia:
        token = await self.autenticacion_neural_repo.autenticarse()
        self.materia_neural_repo.establecer_headers(token)
        materia = await self.materia_neural_repo.guardar_materia(materia)
        return materia

    async def actualizar_materia(self, materia: Materia) -> Materia:
        token = await self.autenticacion_neural_repo.autenticarse()
        self.materia_neural_repo.establecer_headers(token)
        return await self.materia_neural_repo.actualizar_materia(materia)
