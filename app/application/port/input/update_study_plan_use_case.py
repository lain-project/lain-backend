from abc import ABCMeta, abstractmethod
from app.domain.study_plan import StudyPlan

class UpdateStudyPlanQuery(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, study_plan_id: int, study_plan: StudyPlan) -> bool:
        raise NotImplementedError