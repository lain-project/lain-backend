from abc import ABCMeta, abstractmethod

from app.domain.user import User

class CreateUserQuery(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, user: User):
        raise NotImplementedError