from abc import ABCMeta, abstractmethod

class GetUsersQuery(metaclass=ABCMeta):
    @abstractmethod
    def execute(self):
        raise NotImplementedError