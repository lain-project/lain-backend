from abc import ABCMeta, abstractmethod

from app.domain.study_plan import StudyPlan

class CreateStudyPlanQuery(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, study_plan: StudyPlan, years: int):
        raise NotImplementedError