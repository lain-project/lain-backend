from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.study_plan import StudyPlan 

class GetAllStudyPlansQuery(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, filters):
        raise NotImplementedError