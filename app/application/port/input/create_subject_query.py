from abc import ABCMeta, abstractmethod

from app.domain.entities.materia import Materia
from app.shared_kernel.domain.value_objects import UUID


class CreateSubjectQuery(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self, materia: Materia, id_anio_plan_estudio: UUID) -> Materia:
        raise NotImplementedError
