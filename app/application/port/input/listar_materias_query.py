from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.entities.materia import Materia


class ListarMateriasQuery(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self) -> List[Materia]:
        raise NotImplementedError
