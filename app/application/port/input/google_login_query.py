from abc import ABCMeta, abstractmethod
from app.domain.token import Token

class GoogleLoginQuery(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, token_id: str) -> Token:
        raise NotImplementedError