from abc import ABCMeta, abstractmethod

from app.domain.entities.materia import Materia


class ActualizarMateriaQuery(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self, materia: Materia, materia_id: str, anio_plan_estudio_id: int) -> Materia:
        raise NotImplementedError
