from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.entities.anio import Anio


class ObtenerAniosPlanDeEstudioQuery(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self, plan_de_estudio_id: int) -> List[Anio]:
        raise NotImplementedError
