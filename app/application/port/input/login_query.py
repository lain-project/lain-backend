from abc import ABCMeta, abstractmethod
from app.domain.token import Token


class LoginQuery(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self, username: str, password: str) -> Token:
        raise NotImplementedError