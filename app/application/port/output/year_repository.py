from abc import ABCMeta, abstractmethod
from typing import Optional, List
from app.domain.year import Year
from app.domain.study_plan import StudyPlan


class YearRepository(metaclass=ABCMeta):

    @abstractmethod
    async def find_by_id(self, year_id: str) -> Optional[Year]:
        raise NotImplementedError

    @abstractmethod
    async def find_years(self, years) -> List[Year]:
        raise NotImplementedError

    @abstractmethod
    async def save_all(self, study_plan: StudyPlan, years: List[Year]):
        raise NotImplementedError

    @abstractmethod
    async def find_by_id_and_study_plan_id(self, year_id: int, study_plan_id: int) -> Optional[Year]:
        raise NotImplementedError
