from abc import ABCMeta, abstractmethod
# from typing import List
from app.domain.study_plan import StudyPlan

class StudyPlanRepository(metaclass=ABCMeta):

    @abstractmethod
    async def find_by_code(self, code: str) -> StudyPlan:
        raise NotImplementedError

    @abstractmethod
    async def find_by_id(self, id: str) -> StudyPlan:
        raise NotImplementedError

    @abstractmethod
    async def save(self, study_plan: StudyPlan, workspace_id: str):
        raise NotImplementedError

    @abstractmethod
    async def update(self, study_plan: StudyPlan):
        raise NotImplementedError
        
    @abstractmethod
    async def find_all(self):
        raise NotImplementedError