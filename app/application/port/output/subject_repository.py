from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.entities.materia import Materia


class MateriaRepository(metaclass=ABCMeta):

    @abstractmethod
    async def obtener_por_anio_plan_de_estudio(self, anio_plan_de_estudio_id: int) -> List[Materia]:
        raise NotImplementedError

    @abstractmethod
    async def find_by_code(self, code):
        raise NotImplementedError

    @abstractmethod
    async def save(self, subject):
        raise NotImplementedError

    @abstractmethod
    async def crear(self, materia) -> Materia:
        raise NotImplementedError

    @abstractmethod
    async def listar_materias_plan_de_estudio(self, plan_de_estudio_id: int) -> List[Materia]:
        raise NotImplementedError

    @abstractmethod
    async def obtener_por_id(self, materia_id: str) -> Materia:
        raise NotImplementedError

    @abstractmethod
    async def actualizar(self, materia: Materia) -> Materia:
        raise NotImplementedError
