from abc import ABCMeta, abstractmethod
from typing import List
from app.domain.schema import Schema

from app.domain.token_neuralaction import TokenNeuralAction
from app.domain.materia import Materia
from app.domain.year import Year


class NeuralactionsRepository(metaclass=ABCMeta):

    @abstractmethod
    async def authenticate(self) -> TokenNeuralAction:
        raise NotImplementedError

    @abstractmethod
    def save_workspace(self, workspace_name: str, workspace_description: str) -> str:
        raise NotImplementedError

    @abstractmethod
    async def create_schemas(self, workspace_id: str) -> List[Schema]:
        raise NotImplementedError

    @abstractmethod
    def set_headers(self, token: str) -> None:
        raise NotImplemented

    @abstractmethod
    async def update_workspace(self, workspace_id: str, workspace_name: str, workspace_description: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    async def save_subject(self, subject: Materia) -> Materia:
        raise NotImplementedError

    @abstractmethod
    async def save_years(self, workspace_id, years) -> List[Year]:
        raise NotImplementedError
