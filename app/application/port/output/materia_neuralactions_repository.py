from abc import ABCMeta, abstractmethod

from app.domain.entities.materia import Materia


class MateriaNeuralactionsRepository(metaclass=ABCMeta):

    @abstractmethod
    def establecer_headers(self, token):
        raise NotImplementedError

    @abstractmethod
    async def guardar_materia(self, materia: Materia) -> Materia:
        raise NotImplementedError

    @abstractmethod
    async def actualizar_materia(self, materia: Materia) -> Materia:
        raise NotImplementedError
