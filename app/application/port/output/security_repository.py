from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.user_token import UserToken

class SecurityRepository(metaclass=ABCMeta):

    @abstractmethod
    def get_algorithm(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def encode(self, user: UserToken, expire: int) -> str:
        raise NotImplementedError

    @abstractmethod
    def decode(self, token: str) -> UserToken:
        raise NotImplementedError

    @abstractmethod
    def get_hashed_password(self, plain_password: str) -> str:
        raise NotImplementedError