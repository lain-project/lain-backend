from abc import ABCMeta, abstractmethod
from typing import Optional, List

from app.domain.entities.anio import Anio


class AnioRepository(metaclass=ABCMeta):

    @abstractmethod
    async def obtener_por_id(self, id_anio_plan_estudio: int) -> Optional[Anio]:
        raise NotImplementedError

    @abstractmethod
    async def obtener_por_plan_estudio_id(self, plan_estudio_id: int) -> Optional[List[Anio]]:
        raise NotImplementedError
