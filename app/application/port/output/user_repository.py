from abc import ABCMeta, abstractmethod
from typing import List, Optional
from app.domain.user import User

class UserRepository(metaclass=ABCMeta):

    @abstractmethod
    async def find_by_username(self, username: str) -> Optional[User]:
        raise NotImplementedError

    @abstractmethod
    async def find_by_email(self, email: str) -> Optional[User]:
        raise NotImplementedError

    @abstractmethod
    async def create(self, user: User) -> User:
        raise NotImplementedError
    
    @abstractmethod
    async def get_all(self) -> List[User]:
        raise NotImplementedError