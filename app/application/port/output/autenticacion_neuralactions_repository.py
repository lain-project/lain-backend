from abc import ABCMeta, abstractmethod


class AutenticacionNeuralactionsRepository(metaclass=ABCMeta):

    @abstractmethod
    async def autenticarse(self) -> str:
        raise NotImplementedError

