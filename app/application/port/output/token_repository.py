from abc import ABCMeta, abstractmethod

from app.domain.token_neuralaction import TokenNeuralAction

class TokenRepository(metaclass=ABCMeta):

    @abstractmethod
    async def get_token(self) -> TokenNeuralAction:
        raise NotImplementedError

    @abstractmethod
    async def is_expired(self, token: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    async def save_token(self, token: TokenNeuralAction) -> bool:
        raise NotImplementedError
