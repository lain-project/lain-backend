from abc import ABCMeta, abstractmethod
from typing import List

from app.domain.schema import Schema

class SchemaRepository(metaclass=ABCMeta):

    @abstractmethod
    async def save_all(self, schemas: List[Schema], study_plan_id: int):
        raise NotImplementedError