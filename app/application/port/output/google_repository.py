from abc import ABCMeta, abstractmethod

class GoogleRepository(metaclass=ABCMeta):

    @abstractmethod
    def google_verify(self, token_id: str) -> str:
        raise NotImplementedError
