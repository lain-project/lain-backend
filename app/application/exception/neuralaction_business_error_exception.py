from app.application.exception.business_error_exception import BusinessErrorException


class NeuralactionBusinessErrorException(BusinessErrorException):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message 