from app.application.exception.business_error_exception import BusinessErrorException


class ConflictBusinessException(BusinessErrorException):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message