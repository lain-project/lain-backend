from app.application.port.input.update_study_plan_use_case import UpdateStudyPlanQuery
from app.domain.study_plan import StudyPlan
from app.application.port.output.study_plan_repository import StudyPlanRepository
from app.application.exception.conflict_business_exception import ConflictBusinessException
from app.application.service.neuralaction_domain_service import NeuralactionDomainService


class UpdateStudyPlanUseCase(UpdateStudyPlanQuery):

    def __init__(self, study_plan_repository: StudyPlanRepository,\
        neuralactions_domain_service: NeuralactionDomainService) -> None:

        self.study_plan_repository = study_plan_repository
        self.neuralactions_domain_service = neuralactions_domain_service


    async def execute(self, study_plan_id: int, study_plan_update: StudyPlan) -> bool:
        
        exist_study_plan = await self.study_plan_repository.find_by_id(study_plan_id)

        if exist_study_plan is None:
            raise ConflictBusinessException(f"No existe el plan de estudio {study_plan_id}")

        study_plan_by_code = await self.study_plan_repository.find_by_code(study_plan_update.code)

        if study_plan_by_code is not None and study_plan_by_code.id != study_plan_id:
            raise ConflictBusinessException("El código ya se encuentra en uso")
        
        try:
            await self.neuralactions_domain_service.update_workspace(study_plan_update, exist_study_plan.workspace_id)
            await self.study_plan_repository.update(study_plan_update)
        except:
            raise ConflictBusinessException(f"Error al actualizar el plan de estudio {study_plan_update}")
        
        return True