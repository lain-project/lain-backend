
import logging
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
from app.application.port.input.google_login_query import GoogleLoginQuery
from app.application.port.output.google_repository import GoogleRepository
from app.application.port.output.user_repository import UserRepository
from app.application.service.neuralaction_domain_service import NeuralactionDomainService
from app.application.service.security_domain_service import SecurityDomainService
from app.domain.token import Token


class GoogleLoginUseCase(GoogleLoginQuery):

    def __init__(self, google_repository: GoogleRepository,
                    user_repository: UserRepository,
                    security_domain_service: SecurityDomainService,
                    neuralactions_domain_service: NeuralactionDomainService):
        self.google_repository = google_repository
        self.user_repository = user_repository
        self.security_domain_service = security_domain_service
        self.neuralactions_domain_service = neuralactions_domain_service

    async def execute(self, token_id: str) -> Token:
        logging.info(f"Se inicia el proceso de validacion de usuario")
        email = self.google_repository.google_verify(token_id)
        user = await self.user_repository.find_by_email(email)
        if user is None:
            logging.error(f'El usuario con email {email} no existe')
            raise NotFoundBusinessErrorException(f'El usuario con email {email} no existe')
        user.google = True
        token = self.security_domain_service.write_token(user)
        token_neuralaction = await self.neuralactions_domain_service.get_token()
        token.token_neuralaction = token_neuralaction
        logging.info(f"Se genero token de google exitosamente {token.__dict__}")

        return token
    
