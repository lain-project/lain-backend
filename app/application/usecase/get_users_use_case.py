from app.application.port.input.get_users_query import GetUsersQuery
from app.application.port.output.user_repository import UserRepository

class GetUsersUseCase(GetUsersQuery):
    def __init__(self, user_repository: UserRepository):
        self.user_repository = user_repository

    async def execute(self):
        return await self.user_repository.get_all()