import logging
from app.application.exception.conflict_business_exception import ConflictBusinessException
from app.application.port.input.create_user_query import CreateUserQuery
from app.application.port.output.user_repository import UserRepository
from app.application.service.security_domain_service import SecurityDomainService
from app.domain.user import User

class CreateUserUseCase(CreateUserQuery):

    def __init__(self, 
                    user_repository: UserRepository,
                    security_domain_service: SecurityDomainService):
        self.user_repository = user_repository
        self.security_domain_service = security_domain_service

    async def execute(self, user: User) -> User:

        was_username_taken = await self._username_in_use(user.username)

        if was_username_taken:
            logging.error('El usuario ya existe')
            raise ConflictBusinessException("Username already exists")

        was_email_taken = await self._email_in_use(user.email)
        if was_email_taken:
            logging.error('El email ya existe')
            raise ConflictBusinessException("Email already exists")
        
        encript_password = self.security_domain_service.get_password_hash(user.password)
        user.password = encript_password
        return await self.user_repository.create(user)


    async def _username_in_use(self, username):
        return await self.user_repository.find_by_username(username)

    async def _email_in_use(self, email):
        return await self.user_repository.find_by_email(email)