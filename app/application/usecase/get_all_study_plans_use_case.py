import logging
from typing import List
from app.application.port.input.get_all_study_plans_use_case import GetAllStudyPlansQuery
from app.application.port.output.study_plan_repository import StudyPlanRepository

from app.domain.study_plan import StudyPlan 

class GetStudyPlansUseCase(GetAllStudyPlansQuery):
    
    def __init__(self, study_plan_repository: StudyPlanRepository):
        self.study_plan_repository = study_plan_repository

    async def execute(self) :
        return await self.study_plan_repository.find_all()