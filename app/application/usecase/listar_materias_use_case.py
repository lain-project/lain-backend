from typing import List

from app.application.port.output.subject_repository import MateriaRepository

from app.application.port.input.listar_materias_query import ListarMateriasQuery
from app.domain.entities.materia import Materia


class ListarMateriasUseCase(ListarMateriasQuery):

    def __init__(self, materia_repository: MateriaRepository):
        self.materia_repository = materia_repository

    async def execute(self, plan_de_estudio_id: int) -> List[Materia]:
        return await self.materia_repository.listar_materias_plan_de_estudio(plan_de_estudio_id)