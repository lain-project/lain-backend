import logging
from typing import List

from app.application.port.input.create_study_plan_query import CreateStudyPlanQuery
from app.application.port.output.schema_repository import SchemaRepository
from app.application.port.output.study_plan_repository import StudyPlanRepository
from app.domain.study_plan import StudyPlan
from app.application.exception.conflict_business_exception import ConflictBusinessException
from app.application.service.neuralaction_domain_service import NeuralactionDomainService
from app.application.port.output.year_repository import YearRepository
from app.domain.year import Year


class CreateStudyPlanUseCase(CreateStudyPlanQuery):

    def __init__(self, study_plan_repository: StudyPlanRepository, neuralactions_domain_service: NeuralactionDomainService, schemas_repository: SchemaRepository, years_repository: YearRepository):
        self.study_plan_repository = study_plan_repository
        self.neuralactions_domain_service = neuralactions_domain_service
        self.schemas_repository = schemas_repository
        self.years_repository = years_repository

    async def execute(self, study_plan: StudyPlan, number_years: int):
        logging.info(f'Se procede a crear un plan de estudio {study_plan}')

        is_code_unique = await self.study_plan_repository.find_by_code(study_plan.code)

        if is_code_unique is not None:
            logging.error(f'El plan de estudio {study_plan.code} ya existe')
            raise ConflictBusinessException("Study plan code already exists")

        workspace_id = await self.neuralactions_domain_service.create_workspace(study_plan)
        schemas_neuralactions = await self.neuralactions_domain_service.create_schemas(workspace_id)
        study_plan = await self.study_plan_repository.save(study_plan, workspace_id=workspace_id)
        await self.schemas_repository.save_all(schemas_neuralactions, study_plan.id)
        # TODO: IMPLEMENTAR EL CREATE_YEAR_USE_CASE
        schema_year = next(filter(lambda schema: schema.name == 'año', schemas_neuralactions)).schema_id
        logging.info(f'Se procede a crear un año {schema_year}')
        years: List[Year] = await self.years_repository.find_years(number_years)
        [setattr(year, 'schema_id', schema_year) for year in years]
        logging.info(f'Se procede a guardar años en neuralactions {years}')
        years_neural = await self.neuralactions_domain_service.create_years(workspace_id, years)
        [setattr(year, 'node_id', year_neural.node_id) for year, year_neural in zip(years, years_neural)]
        logging.info(f'Se guardaron años existomanete en neuralactions{years}')
        study_plan.years = number_years
        await self.years_repository.save_all(study_plan, years)

        return study_plan