import logging
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException

from app.application.port.input.login_query import LoginQuery
from app.application.port.output.user_repository import UserRepository
from app.application.service.neuralaction_domain_service import NeuralactionDomainService
from app.application.service.security_domain_service import SecurityDomainService
from app.domain.token import Token


class LoginUseCase(LoginQuery):
    def __init__(
        self,
        user_repository: UserRepository,
        security_domain_service: SecurityDomainService,
        neuralactions_domain_service: NeuralactionDomainService,
    ):
        self.user_repository = user_repository
        self.security_domain_service = security_domain_service
        self.neuralactions_domain_service = neuralactions_domain_service

    async def execute(self, username: str, password: str) -> Token:
        logging.info(f"Se procede a logear usuario {username}")
        user = await self.user_repository.find_by_username(username)
        if user is None:
            logging.error(f'El usuario {username} no existe')
            raise NotFoundBusinessErrorException(f'El usuario {username} no existe')

        self.security_domain_service.verify_password(password, user.password)
        token = self.security_domain_service.write_token(user)
        token_neuralaction = await self.neuralactions_domain_service.get_token()
        token.token_neuralaction = token_neuralaction
        logging.info(f'Se realizo autenticacion exitosamente')
        return token