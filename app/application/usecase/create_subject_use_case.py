from app.domain.entities.materia import Materia

from app.application.port.input.create_subject_query import CreateSubjectQuery
from app.application.port.output.subject_repository import MateriaRepository
from app.domain.rules.crear_materias_rules import NoDebeExistirDosMateriasConElMismoCodigo, \
    NoDebeExistirDosMateriasConElMismoNombre
from app.application.port.output.anio_repository import AnioRepository
from app.application.service.materia_domain_service import MateriaDomainService


class CreateSubjectUseCase(CreateSubjectQuery):

    def __init__(self,
                 materia_repository: MateriaRepository,
                 anio_repository: AnioRepository,
                 materia_domain_service: MateriaDomainService):
        self.material_repository = materia_repository
        self.anio_repository = anio_repository
        self.materia_domain_service = materia_domain_service

    async def execute(self, materia: Materia, id_anio_plan_estudio: int) -> Materia:

        materias = await self.material_repository.obtener_por_anio_plan_de_estudio(id_anio_plan_estudio)

        NoDebeExistirDosMateriasConElMismoCodigo(materias, materia.codigo).is_broken()
        NoDebeExistirDosMateriasConElMismoNombre(materias, materia.nombre).is_broken()

        anio = await self.anio_repository.obtener_por_id(id_anio_plan_estudio)
        materia.establecer_anio(anio)
        await self.materia_domain_service.guardar_materia(materia)

        return await self.material_repository.crear(materia)