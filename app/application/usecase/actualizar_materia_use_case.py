import logging

from app.application.service.materia_domain_service import MateriaDomainService

from app.application.port.output.anio_repository import AnioRepository

from app.application.port.output.subject_repository import MateriaRepository

from app.application.port.input.actualizar_materia_query import ActualizarMateriaQuery
from app.domain.entities.materia import Materia
from app.domain.rules.crear_materias_rules import NoDebeExistirDosMateriasConElMismoCodigo, \
    NoDebeExistirDosMateriasConElMismoNombre


class ActualizarMateriaUseCase(ActualizarMateriaQuery):

    def __init__(self,
                 materia_repository: MateriaRepository,
                 anio_repository: AnioRepository,
                 materia_domain_service: MateriaDomainService):
        self.material_repository = materia_repository
        self.anio_repository = anio_repository
        self.materia_domain_service = materia_domain_service

    async def execute(self, datos_actualizados: dict, materia_id: str, anio_plan_estudio_id: int) -> Materia:
        logging.info(f'Actualizando materia con id: {materia_id}')
        logging.info(f'Datos actualizados: {datos_actualizados}')
        logging.info(f'Anio plan de estudio id: {anio_plan_estudio_id}')
        materia_a_actualizar = await self.material_repository.obtener_por_id(materia_id)
        materias = await self.material_repository.obtener_por_anio_plan_de_estudio(anio_plan_estudio_id)
        # if materia_a_actualizar.codigo != datos_actualizados['codigo']:
        #     NoDebeExistirDosMateriasConElMismoCodigo(materias, datos_actualizados['codigo']).is_broken()
        #
        # if materia_a_actualizar.nombre != datos_actualizados['nombre']:
        #     NoDebeExistirDosMateriasConElMismoNombre(materias, datos_actualizados['nombre']).is_broken()

        anio = await self.anio_repository.obtener_por_id(anio_plan_estudio_id)
        materia_a_actualizar.establecer_anio(anio)

        materia_actualizada = self._actualizar_campos(materia_a_actualizar, datos_actualizados)

        await self.materia_domain_service.actualizar_materia(materia_actualizada)
        return await self.material_repository.actualizar(materia_actualizada)

    def _actualizar_campos(self, materia: Materia, datos_actualizados: dict) -> Materia:
        materia.nombre = datos_actualizados['nombre'] if datos_actualizados['nombre'] else materia.nombre
        materia.codigo = datos_actualizados['codigo'] if  datos_actualizados['codigo'] else materia.codigo
        materia.regimen = datos_actualizados['regimen'] if datos_actualizados['regimen'] else materia.regimen
        materia.horas_semanales = datos_actualizados['horas_semanales'] if datos_actualizados['horas_semanales'] else materia.horas_semanales
        materia.horas_totales = datos_actualizados['horas_totales'] if datos_actualizados['horas_totales'] else materia.horas_totales
        logging.warning(f'Materia actualizada: {materia}')
        return materia
