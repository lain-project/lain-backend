from typing import Optional, List

from app.domain.entities.anio import Anio

from app.application.port.output.anio_repository import AnioRepository

from app.application.port.input.obtener_anios_plan_estudio import ObtenerAniosPlanDeEstudioQuery


class ObtenerAniosPlanDeEstudioUseCase(ObtenerAniosPlanDeEstudioQuery):

    def __init__(self, anio_repository: AnioRepository):
        self.anio_repository = anio_repository

    async def execute(self, plan_de_estudio_id) -> Optional[List[Anio]]:
        return await self.anio_repository.obtener_por_plan_estudio_id(plan_de_estudio_id)