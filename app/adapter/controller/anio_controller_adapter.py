import logging
from typing import List

from app.adapter.controller.config import anio_repo_implementation

from app.domain.entities.anio import Anio
from fastapi import APIRouter, Depends
from app.adapter.controller.dependencies.deps import is_admin
from app.application.usecase.obtener_anios_plan_estudio_use_case import ObtenerAniosPlanDeEstudioUseCase

router = APIRouter()


@router.get("/{id_plan_estudio}", status_code=200)
async def get_anio(id_plan_estudio: int) -> List[Anio]:
    logging.info(f"Se procede a buscar el año {id_plan_estudio}")
    caso_de_uso = ObtenerAniosPlanDeEstudioUseCase(anio_repo_implementation)
    anios = await caso_de_uso.execute(id_plan_estudio)
    return anios
