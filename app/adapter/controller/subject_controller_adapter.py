import json
import logging
from datetime import datetime

from app.domain.entities.materia import Materia
from fastapi import APIRouter, Depends, BackgroundTasks
from fastapi.security import OAuth2PasswordBearer

from app.adapter.controller.serializadores.materia_serializer import MateriaSerializer
from app.adapter.controller.config import subject_repo_implementation, \
    anio_repo_implementation, materia_domain_service, auditoria_implementation
from app.adapter.controller.dependencies.deps import is_admin, get_current_user
from app.adapter.controller.model.subject_rest import MateriaACrear, MateriaAActualizar
from app.application.usecase.create_subject_use_case import CreateSubjectUseCase
from app.shared_kernel.domain.value_objects import UUID
from app.application.usecase.listar_materias_use_case import ListarMateriasUseCase
from app.application.usecase.actualizar_materia_use_case import ActualizarMateriaUseCase

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def auditar_materia(materia: Materia, usuario_id: int, evento: str, estado: str):
    auditoria = {
        "id": str(UUID.v4()),
        "evento": evento,
        "estado": estado,
        "payload": MateriaSerializer.serialize(materia),
        "usuario": usuario_id,
        "fecha_creacion": datetime.now(),
        "fecha_actualizacion": datetime.now()
    }
    logging.info(f'se audita {auditoria}')
    await auditoria_implementation.guardar_auditoria(auditoria)


@router.post("", status_code=201)
async def crear_materia(background_tasks: BackgroundTasks, materia_a_crear: MateriaACrear, admin=Depends(is_admin), \
                        user=Depends(get_current_user)):
    id_anio_plan_de_estudio = materia_a_crear.anio_plan_de_estudio
    materia = materia_a_crear.to_domain()
    caso_de_uso = CreateSubjectUseCase(subject_repo_implementation, anio_repo_implementation, materia_domain_service)
    result = await caso_de_uso.execute(materia, id_anio_plan_de_estudio)
    background_tasks.add_task(auditar_materia, result, user.id, "Crear Materia", "Exitoso")
    return result is not None


@router.get("/{plan_de_estudio}", status_code=200)
async def listar_materias(plan_de_estudio: int, user=Depends(get_current_user)):
    caso_de_uso = ListarMateriasUseCase(subject_repo_implementation)
    materias = await caso_de_uso.execute(plan_de_estudio)
    return [json.loads(MateriaSerializer.serialize(materia)) for materia in materias]


@router.put("/{materia_id}", status_code=200)
async def actualizar_materia(background_tasks: BackgroundTasks, materia_id: str, materia_a_actualizar: MateriaAActualizar, \
                             admin=Depends(is_admin), user=Depends(get_current_user)):
    id_anio_plan_de_estudio = materia_a_actualizar.anio_plan_de_estudio
    datos_actualizar = materia_a_actualizar.to_domain()
    caso_de_uso = ActualizarMateriaUseCase(subject_repo_implementation, \
                                           anio_repo_implementation, \
                                           materia_domain_service)
    result = await caso_de_uso.execute(datos_actualizar, materia_id, id_anio_plan_de_estudio)
    # background_tasks.add_task(auditar_materia, result, user.id, "Actualizar Materia", "Exitoso")
    return json.loads(MateriaSerializer.serialize(result))
