import logging
import json
from fastapi import APIRouter

from app.application.usecase.google_login_use_case import GoogleLoginUseCase
from app.application.usecase.login_use_case import LoginUseCase
from app.adapter.controller.model.user_rest import UserRest
from app.adapter.controller.model.token_rest import TokenRest
from app.adapter.controller.config import (
    user_repo_implementation,
    google_repo_implementation,
    neuralactions_domain_service,
    security_domain_service,
)

router = APIRouter()


@router.post("/login", response_model=TokenRest, status_code=201)
async def login(user: UserRest) -> TokenRest:
    logging.info(f'Se procede a generar el token para el usuario {json.dumps(user.__dict__)}')
    use_case = LoginUseCase(user_repo_implementation, security_domain_service, neuralactions_domain_service)
    token = await use_case.execute(user.username, user.password)
    logging.info(f'Se realizo proceso de logeo exitosamente')
    return TokenRest(access_token=token.access_token, token_neuralaction=token.token_neuralaction)


@router.post("/google", status_code=201)
async def google(token_id: str) -> TokenRest:

    logging.info(f"Se procede a loguear por google el usuario con token: {token_id}")
    use_case = GoogleLoginUseCase(
        google_repo_implementation,
        user_repo_implementation,
        security_domain_service,
        neuralactions_domain_service,
    )
    token = await use_case.execute(token_id)
    logging.info(f"Se genero token exitosamente")
    return TokenRest(access_token=token.access_token, token_neuralaction=token.token_neuralaction)
