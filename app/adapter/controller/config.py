from app.adapter.database.auditoria_database_adapter import AuditoriaDatabaseAdapter
from app.adapter.rest.materia_neuralaction_rest_adapter import MateriaNeuralActionRestAdapter
from app.adapter.rest.autenticacion_neuralaction_rest_adapter import AutenticacionNeuralactionsRestAdapter
from app.adapter.database.anio_database_adapter import AnioDatabaseAdapter
from app.adapter.database.subject_database_adapter import MateriaDatabaseAdapter

from app.adapter.database.year_database_adapter import YearDatabaseAdapter

from app.adapter.database.study_plan_database_adapter import StudyPlanDatabaseAdapter
from app.adapter.database.token_database_adapter import TokenDatabaseAdapter
from app.adapter.database.user_database_adapter import UserDatabaseAdapter
from app.adapter.database.schema_database_adapter import SchemaDatabaseAdapter

from app.adapter.rest.neuralactions_rest_adapter import NeuralactionsRestAdapter

from app.adapter.security.google_security_adapter import GoogleSecurityAdapter
from app.adapter.security.security_adapter import SecurityAdapter

from app.application.service.neuralaction_domain_service import NeuralactionDomainService
from app.application.service.security_domain_service import SecurityDomainService
from app.adapter.database.database import async_session
import aiohttp
import ujson

from app.application.service.materia_domain_service import MateriaDomainService

client_session = aiohttp.ClientSession(json_serialize=ujson.dumps)
user_repo_implementation = UserDatabaseAdapter(async_session())
google_repo_implementation = GoogleSecurityAdapter()
neuralactions_repo_implementation = NeuralactionsRestAdapter(client_session)
year_repo_implementation = YearDatabaseAdapter(async_session())
study_plan_repo_implementation = StudyPlanDatabaseAdapter(async_session())
schemas_repo_implementation = SchemaDatabaseAdapter(async_session())
subject_repo_implementation = MateriaDatabaseAdapter(async_session())
anio_repo_implementation = AnioDatabaseAdapter(async_session())
auditoria_implementation = AuditoriaDatabaseAdapter(async_session())

neuralactions_domain_service = NeuralactionDomainService(TokenDatabaseAdapter(async_session()),
                                                         NeuralactionsRestAdapter(client_session))
security_domain_service = SecurityDomainService(SecurityAdapter())
token_repo_implementation = TokenDatabaseAdapter(async_session())
materia_domain_service = MateriaDomainService(token_repo_implementation, AutenticacionNeuralactionsRestAdapter(async_session(),token_repo_implementation), \
                                              MateriaNeuralActionRestAdapter(client_session, async_session()))
