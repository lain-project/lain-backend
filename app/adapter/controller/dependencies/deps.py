import logging
import traceback
from app.application.exception.business_error_exception import BusinessErrorException
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
from app.domain.enums.rol_name_enum import RolNameEnum
from app.domain.user import User
from app.adapter.security.security_adapter import SecurityAdapter
from app.adapter.database.user_database_adapter import UserDatabaseAdapter
from app.config.config import settings
from fastapi import HTTPException, status, Request, Depends
from fastapi.security import OAuth2PasswordBearer
from app.adapter.database.database import async_session


reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.ROUTE}/auth/login"
)
user_repo_implementation = UserDatabaseAdapter(async_session())


async def get_current_user(token: str = Depends(reusable_oauth2)) -> User:
    try:
        logging.info(f'Se procede a obtener el usuario logeado')
        payload = SecurityAdapter().decode(token)
        user = await user_repo_implementation.find_by_username(payload['username'])
    
    except NotFoundBusinessErrorException:
        logging.error(f'El usuario {payload.username} no existe')
        raise HTTPException(status_code=404, detail="User not found")

    except BusinessErrorException:
        logging.error(f'Error en la base de datos {traceback.format_exc()}') 
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
    return user


def is_admin(user = Depends(get_current_user)) -> bool:

    if(user.rol.name != RolNameEnum.ADMIN.value):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Invalid role")
    return True
    