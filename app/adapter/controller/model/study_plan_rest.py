from pydantic import BaseModel
from app.domain.study_plan import StudyPlan


class StudyPlanRest(BaseModel):
    id: int | None
    code: str | None
    name: str | None
    description: str | None
    workspace_id: int | None
    years: int | None

    @property
    def to_domain(self) -> StudyPlan:
        return StudyPlan(
            id=self.id,
            code=self.code,
            name=self.name,
            description=self.description,
            workspace_id=self.workspace_id,
            years=self.years
        )
