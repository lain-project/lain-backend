from typing import Optional
from pydantic import BaseModel


class TokenRest(BaseModel):
    access_token: Optional[str]
    refresh_token: Optional[str]
    token_neuralaction: Optional[str]
