from typing import Optional

from app.domain.value_objects.regimen import Regimen

from app.domain.value_objects.codigo import Codigo

from app.domain.value_objects.nombre import Nombre
from pydantic import BaseModel, validator
from app.domain.entities.materia import Materia
from app.domain.factories.materia_factory import MateriaFactory


class SubjectRest(BaseModel):
    id: Optional[int]
    image: Optional[str]
    name: Optional[str]
    code: Optional[str]
    regime: Optional[str]
    weekly_hours: Optional[int]
    total_hours: Optional[int]
    study_plan_id: Optional[int]
    study_plan_year_id: Optional[int]

    @property
    def to_domain(self) -> Materia:
        return Materia(
            id=self.id,
            image=self.image,
            name=self.name,
            code=self.code,
            regime=self.regime,
            weekly_hours=self.weekly_hours,
            total_hours=self.total_hours,
            study_plan_id=self.study_plan_id,
            year_id=self.year_id
        )


class MateriaACrear(BaseModel):
    nombre: str
    codigo: str
    regimen: str
    anio_plan_de_estudio: int
    horas_semanales: Optional[int]
    horas_totales: Optional[int]

    def to_domain(self) -> Materia:
        materia = MateriaFactory.crear_materia_base(
            self.nombre,
            self.codigo,
            self.regimen,
        )
        materia.horas_totales = self.horas_totales
        materia.horas_semanales = self.horas_semanales
        return materia


class MateriaAActualizar(BaseModel):
    id: Optional[str] = None
    nombre: Optional[str] = None
    codigo: Optional[str] = None
    regimen: Optional[str] = None
    anio_plan_de_estudio: Optional[int] = None
    horas_semanales: Optional[int] = None
    horas_totales: Optional[int] = None

    def to_domain(self) -> dict:
        materia = {}
        materia['id'] = self.id
        materia['nombre'] = Nombre(self.nombre) if self.nombre else None
        materia['codigo'] = Codigo(self.codigo) if self.codigo else None
        materia['regimen'] = Regimen(self.regimen) if self.regimen else None
        materia['horas_semanales'] = self.horas_semanales
        materia['horas_totales'] = self.horas_totales
        return materia


