from typing import Optional
from pydantic import BaseModel
from app.domain.profile import Profile
from app.domain.rol import Rol

from app.domain.user import User

class UserRest(BaseModel):
    username: str
    password: str


class UserCreateRest(UserRest):
    email: str
    lastname: str
    firstname: str
    position: Optional[str]
    avatar: Optional[str]
    rol_id: int

    def to_domain(self):
        profile = Profile(self.lastname, self.firstname, self.position)
        rol = Rol(self.rol_id)
        return User(
            username=self.username,
            password=self.password,
            avatar=self.avatar,
            email=self.email,
            rol=rol,
            profile=profile
        )

class UserProfileRest(BaseModel):
    id: int
    username: str
    email: str
    lastname: str
    firstname: str
    rol_id: str
    rol_name: str
    status: int
    avatar: Optional[str]
    position: Optional[str]