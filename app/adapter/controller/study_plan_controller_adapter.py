import logging
from app.adapter.controller.dependencies.deps import is_admin
from app.adapter.controller.model.study_plan_rest import StudyPlanRest
from app.application.usecase.create_study_plan_use_case import CreateStudyPlanUseCase
from app.application.usecase.update_study_plan_use_case import UpdateStudyPlanUseCase
from app.adapter.controller.config import study_plan_repo_implementation, neuralactions_repo_implementation, \
    neuralactions_domain_service, schemas_repo_implementation, year_repo_implementation
from app.application.usecase.get_all_study_plans_use_case import GetStudyPlansUseCase
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordBearer


router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@router.post("", status_code=201)
async def create_study_plan(study_plan: StudyPlanRest, admin=Depends(is_admin)):
    logging.info(f"Se procede a crear un plan de estudio {study_plan}")
    years = study_plan.years
    study_plan = study_plan.to_domain
    use_case = CreateStudyPlanUseCase(study_plan_repo_implementation,
                                      neuralactions_domain_service,
                                      schemas_repo_implementation,
                                      year_repo_implementation)
    result = await use_case.execute(study_plan, years)
    logging.info(f"se creo el plan de estudio {result}")
    return result is not None


@router.get("", status_code=200)
async def get_studies_plans():
    logging.info(f"Se procede a buscar todos los planes de estudio")
    use_case = GetStudyPlansUseCase(study_plan_repo_implementation)
    result =  await use_case.execute()
    logging.info(f"se buscaron los planes de estudio exitosamente")
    return result


@router.put("/{study_plan_id}", status_code=200)
async def update_study_plan(study_plan_id:int, study_plan: StudyPlanRest, admin=Depends(is_admin)):
    logging.info(f"Se procede a actualizar plan de estudio {study_plan_id}")
    use_case = UpdateStudyPlanUseCase(study_plan_repo_implementation, neuralactions_domain_service)
    result = await use_case.execute(study_plan_id, study_plan.to_domain)
    logging.info(f"se actualizo el plan de estudio {result}")
    return result