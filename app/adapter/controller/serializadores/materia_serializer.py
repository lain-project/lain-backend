import json

from app.domain.entities.materia import Materia


class MateriaSerializer:

    @staticmethod
    def serialize(material: Materia):
        return json.dumps({
            'id': str(material.id),
            'nombre': material.obtener_nombre(),
            'codigo': material.obtener_codigo(),
            'regimen': material.obtener_regimen(),
            'anio': material.anio.id,
            'horas_semanales': material.horas_semanales,
            'horas_totales': material.horas_totales
        })

    @staticmethod
    def serialize_sin_anio(materia: Materia):
        return json.dumps({
            'id': str(materia.id),
            'nombre': materia.obtener_nombre(),
            'codigo': materia.obtener_codigo(),
            'regimen': materia.obtener_regimen(),
            'horas_semanales': materia.horas_semanales,
            'horas_totales': materia.horas_totales
        })
