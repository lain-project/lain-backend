import logging
from app.application.usecase.create_user_use_case import CreateUserUseCase
from app.application.usecase.get_users_use_case import GetUsersUseCase
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordBearer
from app.adapter.controller.dependencies.deps import is_admin
from app.adapter.controller.model.user_rest import (
    UserCreateRest,
    UserProfileRest,
)
from app.domain.user import User
from app.adapter.controller.config import (
    user_repo_implementation,
    security_domain_service,
)

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@router.post("", status_code=201)
async def create_user(user: UserCreateRest, admin=Depends(is_admin)) -> bool:

    user = user.to_domain()
    use_case = CreateUserUseCase(user_repo_implementation, security_domain_service)
    result = await use_case.execute(user)
    logging.info(f"Usuario {user.username} creado exitosamente")

    return result is not None


@router.get("", status_code=200)
async def get_users():
    logging.info(f"Se procede a obtener todos los usuarios")
    use_case = GetUsersUseCase(user_repo_implementation)
    users = await use_case.execute()
    result = [ map_to_rest(user) for user in users]
    logging.info(f"Usuarios obtenidos exitosamente {users}")
    return result


def map_to_rest(user: User):
    return UserProfileRest(
        id=user.id,
        username=user.username,
        email=user.email,
        firstname=user.profile.firstname,
        lastname=user.profile.lastname,
        position=user.profile.position,
        rol_id = user.rol.id,
        rol_name = user.rol.name,
        status=user.status,
        avatar=user.avatar
    )