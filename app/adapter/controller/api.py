from fastapi import APIRouter
from app.adapter.controller.auth_controller_adapter import router as auth_router
from app.adapter.controller.user_controller_adapter import router as user_router
from app.adapter.controller.study_plan_controller_adapter import router as study_plan_router
from app.adapter.controller.subject_controller_adapter import router as subject_router
from app.adapter.controller.anio_controller_adapter import router as anio_router
api_router = APIRouter()

api_router.include_router(auth_router, prefix="/auth", tags=["Authorization"])
api_router.include_router(user_router, prefix="/users", tags=["Users"])
api_router.include_router(study_plan_router, prefix="/study-plan", tags=["Study plan"])
api_router.include_router(subject_router, prefix="/subject", tags=["Subject"])
api_router.include_router(anio_router, prefix="/anio", tags=["Año"])