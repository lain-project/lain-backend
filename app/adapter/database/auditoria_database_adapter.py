import logging

from app.adapter.database.models.auditoria_model import AuditoriaModel


class AuditoriaDatabaseAdapter:

    def __init__(self, session):
        self.session = session

    async def guardar_auditoria(self, auditoria):
        logging.info('Guardando auditoria')
        logging.warning(auditoria['evento'])
        logging.warning(auditoria['estado'])
        logging.warning(auditoria['fecha_creacion'])
        logging.warning(auditoria['fecha_actualizacion'])
        logging.warning(auditoria['payload'])
        logging.warning(auditoria['usuario'])
        auditoria_model = AuditoriaModel(evento= auditoria['evento'], estado=auditoria['estado'],
                                         fecha_creacion=auditoria['fecha_creacion'],
                                         fecha_actualizacion=auditoria['fecha_actualizacion'],
                                         payload=str(auditoria['payload']), usuario_id=auditoria['usuario'])
        self.session.add(auditoria_model)
        await self.session.commit()

