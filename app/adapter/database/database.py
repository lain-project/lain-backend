import logging

from sqlalchemy import Table, Column, ForeignKey

from app.config.config import settings

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
logging.info(f'Inicio db {settings.SQLALCHEMY_DATABASE_URI}')
engine = create_async_engine(settings.SQLALCHEMY_DATABASE_URI)
async_session = sessionmaker(bind=engine, class_=AsyncSession )
# async_session = AsyncSession(engine, expire_on_commit=False)

async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session
