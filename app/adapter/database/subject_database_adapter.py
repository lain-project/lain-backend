import asyncio
import logging
import traceback
from typing import Optional, List

from app.tests.utils.coroutines import async_return

from app.adapter.database.models.year_model import YearModel

from app.domain.factories.anio_factory import AnioFactory

from app.adapter.database.models.study_plan_model import StudyPlanModel

from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel
from sqlalchemy import select, join
from sqlalchemy.orm import joinedload, selectinload

from app.adapter.database.mappers.subject_db_mapper import SubjectDBMapper
from app.adapter.database.models.subject_model import SubjectModel
from app.application.port.output.subject_repository import MateriaRepository
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.domain.entities.materia import Materia
from app.domain.factories.materia_factory import MateriaFactory


class MateriaDatabaseAdapter(MateriaRepository):

    def __init__(self, session) -> None:
        self.session = session

    def find_by_code(self, code: str) -> Optional[Materia]:
        logging.info(f'Se procede a buscar materia en la base de datos por código {code}')
        try:
            statement = select(SubjectModel).where(SubjectModel.code == code)
            result = self.session.execute(statement)
            subject: SubjectModel = result.scalars().first()
            return SubjectDBMapper.to_domain(subject)
        except Exception:
            logging.error(f'Error al buscar materia en la base de datos por código {code}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def save(self, subject: Materia):
        logging.info(f'Se procede a guardar materia en la base de datos {subject}')
        try:
            subject_model = SubjectModel(code=subject.code, name=subject.name, image='example.png',
                                         weekly_hours=subject.weekly_hours, total_hours=subject.total_hours,
                                         study_plan_year_id=subject.year.study_plan_year, regime=subject.regime)
            self.session.add(subject_model)
            await self.session.commit()
            statement = select(SubjectModel).where(SubjectModel.code == subject.code)
            result = await self.session.execute(statement)
            subject: SubjectModel = result.scalars().first()
            logging.info(f'Materia guardada en la base de datos {subject}')
            return subject
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def obtener_por_anio_plan_de_estudio(self, anio_plan_de_estudio_id: int) -> List[Materia]:
        logging.info(
            f'Se procede a buscar materias en la base de datos por año de plan de estudio {anio_plan_de_estudio_id}')
        try:
            statement = select(SubjectModel) \
                .where(SubjectModel.study_plan_year_id == anio_plan_de_estudio_id)
            result = await self.session.execute(statement)
            subjects: List[SubjectModel] = result.scalars()
            subject_domain = [MateriaFactory.crear_materia_base(subject.name, subject.code, subject.regime.upper()) for
                              subject in subjects]
            return subject_domain
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def crear(self, materia: Materia) -> Materia:
        logging.info(f'Se procede a guardar materia en la base de datos {materia}')
        try:
            query = select(SubjectModel).where(SubjectModel.id == str(materia.id))
            materia_db = await self.session.execute(query)
            materia_db = materia_db.scalars().first()
            materia_db.code = materia.obtener_codigo()
            materia_db.name = materia.obtener_nombre()
            materia_db.regime = materia.obtener_regimen()
            materia_db.image = 'example.png'
            materia_db.weekly_hours = materia.horas_semanales
            materia_db.total_hours = materia.horas_totales
            materia_db.study_plan_year_id = materia.anio.id
            await self.session.commit()
            logging.info(f'Materia guardada en la base de datos {materia_db}')
            return materia
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def listar_materias_plan_de_estudio(self, plan_de_estudio_id: int) -> List[Materia]:
        logging.info(f'Se procede a buscar materias en la base de datos por plan de estudio {plan_de_estudio_id}')
        try:
            statement = select(SubjectModel) \
                .join(StudyPlanYearModel, SubjectModel.study_plan_year_id == StudyPlanYearModel.id) \
                .join(StudyPlanModel, StudyPlanYearModel.study_plan_id == StudyPlanModel.id) \
                .where(StudyPlanModel.id == plan_de_estudio_id) \
                .options(selectinload(SubjectModel.study_plan_year))
            result = await self.session.execute(statement)
            subjects: List[SubjectModel] = result.scalars().all()
            # logging.warning(f'Materias encontradas en la base de datos {str(subjects.__dict__)}')
            subject_domain = [self._unificar_datos(subject) for subject in subjects]
            subject_domain = await asyncio.gather(*subject_domain)
            return subject_domain
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def _unificar_datos(self, materia_model):
        study_plan_year = materia_model.study_plan_year_id
        anio_id = materia_model.study_plan_year.year_id
        statement_year = select(YearModel).where(YearModel.id == anio_id)
        anio_model = await self.session.execute(statement_year)
        anio_model: YearModel = anio_model.scalars().first()

        anio = AnioFactory.crear_anio_sin_plan_de_estudio(study_plan_year, anio_model.numeration, anio_model.name)
        materia = MateriaFactory.crear_materia_completa(materia_model.id, materia_model.name, materia_model.code,
                                                        materia_model.regime.upper(), materia_model.weekly_hours,
                                                        materia_model.total_hours)
        materia.establecer_anio(anio)
        return materia

    async def obtener_por_id(self, materia_id: str):
        try:
            logging.info(f'Materia a buscar {materia_id}')
            statement = select(SubjectModel).where(SubjectModel.id == materia_id)
            result = await self.session.execute(statement)
            materia_model: SubjectModel = result.scalars().first()
            logging.info(f'Materia encontrada {materia_model}')
            materia = MateriaFactory.crear_materia_completa(materia_model.id, materia_model.name, materia_model.code,
                                                            materia_model.regime, materia_model.weekly_hours,
                                                            materia_model.total_hours)
            return materia
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def actualizar(self, materia: Materia) -> Materia:
        logging.info(f'Se procede a actualizar materia en la base de datos {materia}')
        try:
            query = select(SubjectModel).where(SubjectModel.id == str(materia.id))
            materia_db = await self.session.execute(query)
            materia_db = materia_db.scalars().first()
            materia_db.code = materia.obtener_codigo()
            materia_db.name = materia.obtener_nombre()
            materia_db.regime = materia.obtener_regimen()
            materia_db.image = 'example.png'
            materia_db.weekly_hours = materia.horas_semanales
            materia_db.total_hours = materia.horas_totales
            materia_db.study_plan_year_id = materia.anio.id
            await self.session.commit()
            logging.info(f'Materia actualizada en la base de datos {materia_db}')
            return materia
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
