from app.domain.study_plan import StudyPlan
from app.adapter.database.models.study_plan_model import StudyPlanModel
import logging


class StudyPlanDBMapper:

    @staticmethod
    def to_domain(study_plan_model: StudyPlanModel) -> StudyPlan | None:
        logging.info(f"Se procede a mapear plan de estudio model a plan de estudio")

        return StudyPlan(
            id=study_plan_model.id,
            code=study_plan_model.code,
            name=study_plan_model.name,
            description=study_plan_model.description,
            workspace_id=study_plan_model.workspace_id,
        )