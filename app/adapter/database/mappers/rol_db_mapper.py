from app.domain.rol import Rol
from app.adapter.database.models.rol_model import RolModel

class RolDBMapper:

    @staticmethod
    def to_domain(rol_model: RolModel) -> Rol:
        return Rol(rol_model.id, rol_model.name, rol_model.description)