from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel

from app.domain.year import Year


class StudyPlanYearDBMapper:

    @staticmethod
    def to_domain_year(study_plan_year: StudyPlanYearModel):
        year = study_plan_year.years
        study_plan = study_plan_year.study_plan
        return Year(
            id = year.id,
            name= year.name,
            numeration=year.numeration,
            schema_id=None,
            study_plan_id=study_plan.id,
            workspace_id=study_plan.workspace_id,
            node_id=study_plan_year.node_id,
            study_plan_year=study_plan_year.id
        )