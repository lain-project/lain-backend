from app.domain.materia import Materia

from app.adapter.database.models.subject_model import SubjectModel


class SubjectDBMapper:

    @staticmethod
    def to_domain(subject: SubjectModel) -> Materia:
        if subject is None:
            return None
        if subject.study_plan_year is None:
            year = None
        else:
            year = subject.study_plan_year.year


        return Materia(
            id=subject.id,
            image=subject.image,
            name=subject.name,
            code=subject.code,
            regime=subject.regime,
            weekly_hours=subject.weekly_hours,
            total_hours=subject.total_hours,
            study_plan_id=subject.study_plan_id,
            study_plan=subject.study_plan,
            year=year
        )


    @staticmethod
    def to_model(subject: Materia) -> SubjectModel:
        if subject is None:
            return None
        if subject.study_plan is None:
            study_plan = None
        else:
            study_plan = subject.study_plan.id

        if subject.year is None:
            year = None
        else:
            year = subject.year.id

        return SubjectModel(
            id=subject.id,
            image=subject.image,
            name=subject.name,
            code=subject.code,
            regime=subject.regime,
            weekly_hours=subject.weekly_hours,
            total_hours=subject.total_hours,
            study_plan_id=study_plan,
            study_plan=subject.study_plan,
            year_id=year,
            year=subject.year
        )