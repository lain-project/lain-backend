from app.adapter.database.mappers.profile_db_mapper import ProfileDBMapper
from app.adapter.database.mappers.rol_db_mapper import RolDBMapper
from app.domain.user import User
from app.adapter.database.models.user_model import UserModel
import logging



class UserDBMapper:

    @staticmethod
    def to_domain(user_model: UserModel) -> User | None:
        logging.info(f"Se procede a mapear user model a user")
        logging.info(f'Rol {user_model.rol}')
        if user_model is None:
            return None
        rol = RolDBMapper.to_domain(user_model.rol) if user_model.rol else None
        profile = ProfileDBMapper.to_domain(user_model.profile) if user_model.profile else None

        return User(
                username=user_model.username,
                email=user_model.email,
                password=user_model.password,
                id=user_model.id,
                avatar=user_model.avatar,
                google=user_model.google,
                status=user_model.status,
                rol = rol,
                profile = profile 
            )
