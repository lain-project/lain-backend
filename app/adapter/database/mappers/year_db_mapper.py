from app.adapter.database.models.year_model import YearModel
from app.domain.year import Year


class YearDBMapper:

    @staticmethod
    def to_domain(year_model: YearModel) -> Year:
        return Year(
            id = year_model.id,
            name= year_model.name,
            numeration=year_model.numeration,
            schema_id=None
        )