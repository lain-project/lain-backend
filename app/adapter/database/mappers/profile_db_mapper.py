from app.domain.profile import Profile
from app.adapter.database.models.profile_model import ProfileModel

class ProfileDBMapper:

    @staticmethod
    def to_domain(profile_model: ProfileModel) -> Profile:
        return Profile(
            firstname=profile_model.firstname,
            lastname=profile_model.lastname,
            position=profile_model.position
        )