from sqlalchemy import Column, String

from app.adapter.database.database import Base
from sqlalchemy.dialects.postgresql import UUID
import uuid


class RelacionModel(Base):

    __tablename__ = 'relaciones_neural'
    
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    nodo_inicial = Column('nodo_inicial', String)
    nodo_final = Column('nodo_final', String)
