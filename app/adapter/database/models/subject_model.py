import uuid

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from app.adapter.database.database import Base


class SubjectModel(Base):

    __tablename__ = 'subjects'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    image = Column(String)
    name = Column(String)
    code = Column(String)
    regime = Column(String)
    weekly_hours = Column(Integer)
    total_hours = Column(Integer)
    node_id = Column(String)

    study_plan_year_id = Column('study_plan_year',Integer, ForeignKey('study_plans_years.id'))

    study_plan_year = relationship("StudyPlanYearModel", back_populates="subjects")
    nodo_materia = relationship("NodoMateriaModel", back_populates="materia")


    __mapper_args__ = {"eager_defaults": True}
