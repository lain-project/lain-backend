from app.adapter.database.database import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

class ProfileModel(Base):
    __tablename__ = 'profiles'
    
    id = Column(Integer, primary_key=True, index=True)
    firstname = Column(String)
    lastname = Column(String)
    position = Column(String)
    user = relationship("UserModel", back_populates="profile")
    
    __mapper_args__ = {"eager_defaults": True}
