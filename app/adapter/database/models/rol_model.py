from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.adapter.database.database import Base


class RolModel(Base):

    __tablename__ = 'rols'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    status = Column(Integer)
    user = relationship("UserModel", back_populates="rol")

    __mapper_args__ = {"eager_defaults": True}
