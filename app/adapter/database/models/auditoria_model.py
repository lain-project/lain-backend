import uuid
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from app.adapter.database.database import Base
from app.adapter.database.models.user_model import UserModel


class AuditoriaModel(Base):
    __tablename__ = 'auditoria'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    evento = Column(String)
    estado = Column(String)
    fecha_creacion = Column(DateTime)
    fecha_actualizacion = Column(DateTime)
    payload = Column(String)
    usuario_id = Column('usuario_id', Integer, ForeignKey('users.id'))

    usuario = relationship("UserModel", back_populates="auditoria")
