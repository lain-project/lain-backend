from sqlalchemy import Column, ForeignKey, Integer, String
from app.adapter.database.database import Base
from sqlalchemy.orm import relationship
from app.adapter.database.models.study_plan_model import StudyPlanModel

class SchemaModel(Base):
    __tablename__ = 'schemas'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    schema_id = Column(String)

    study_plan_id = Column('study_plan_id',Integer, ForeignKey('study_plans.id'))
    study_plan: StudyPlanModel = relationship("StudyPlanModel", back_populates="schema")

    __mapper_args__ = {"eager_defaults": True}
