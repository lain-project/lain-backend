from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.adapter.database.database import Base

class YearModel(Base):
    __tablename__ = 'years'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    numeration = Column(Integer)

    study_plans_years = relationship("StudyPlanYearModel", back_populates="years")

    __mapper_args__ = {"eager_defaults": True}