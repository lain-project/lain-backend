from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from app.adapter.database.database import Base
from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel

class StudyPlanModel(Base):
    __tablename__ = 'study_plans'
    id = Column(Integer, primary_key=True)
    code = Column(String)
    name = Column(String)
    description = Column(String)
    workspace_id = Column(String)
    schema = relationship("SchemaModel", back_populates="study_plan")
    study_plan_year = relationship("StudyPlanYearModel", back_populates="study_plan")

    __mapper_args__ = {"eager_defaults": True}