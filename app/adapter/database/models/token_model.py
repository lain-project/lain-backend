
from app.adapter.database.database import Base
from sqlalchemy import Column, DateTime, Integer, String

class TokenModel(Base):
    __tablename__ = 'na_tokens'
    id = Column(Integer, primary_key=True)
    access_token = Column(String)
    token_type = Column(String)
    user_id = Column(Integer)
    expires_at = Column(DateTime)

