from datetime import datetime
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime
from app.adapter.database.database import Base
from sqlalchemy.orm import relationship
from app.adapter.database.models.profile_model import ProfileModel
from app.adapter.database.models.rol_model import RolModel


class UserModel(Base): 
        __tablename__ = 'users'
        
        id = Column(Integer, primary_key=True)
        username = Column(String, nullable=False, unique=True)
        email = Column(String(40), nullable=False, unique=True)
        password = Column(String, nullable=False) 
        avatar = Column(String, nullable=True) 
        google = Column(Boolean)
        status = Column(Integer)
        created_at = Column(DateTime, default=datetime.now())
        updated_at = Column(DateTime, default=datetime.now())
        created_by = Column(Integer)
        updated_by= Column(Integer)
        
        rol_id = Column('rol_id',Integer, ForeignKey("rols.id"))
        profile_id = Column('profile_id',Integer, ForeignKey("profiles.id"))

        rol: RolModel = relationship("RolModel", back_populates="user")
        profile: ProfileModel = relationship("ProfileModel", back_populates="user")
        auditoria = relationship("AuditoriaModel", back_populates="usuario")

        __mapper_args__ = {"eager_defaults": True}