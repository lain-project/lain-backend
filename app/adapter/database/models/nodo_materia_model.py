import uuid
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from app.adapter.database.database import Base
from app.adapter.database.models.subject_model import SubjectModel


class NodoMateriaModel(Base):
    __tablename__ = 'nodos_materias'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    nombre = Column(String)
    nodo_id = Column(String)

    materia_id = Column('materia_id', UUID(as_uuid=True), ForeignKey('subjects.id'))

    materia = relationship("SubjectModel", back_populates="nodo_materia")
