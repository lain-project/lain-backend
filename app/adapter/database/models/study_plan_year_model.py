from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from app.adapter.database.models.year_model import YearModel
from app.adapter.database.models.subject_model import SubjectModel
from app.adapter.database.database import Base

class StudyPlanYearModel(Base):
    __tablename__ = 'study_plans_years'

    id = Column(Integer, primary_key=True)
    node_id = Column(String, nullable=False)
    year_id = Column('year_id', Integer, ForeignKey('years.id'))
    study_plan_id = Column('study_plan_id', Integer, ForeignKey('study_plans.id'))

    years = relationship("YearModel", back_populates="study_plans_years")
    study_plan = relationship("StudyPlanModel", back_populates="study_plan_year")
    subjects = relationship("SubjectModel", back_populates="study_plan_year")

    __mapper_args__ = {"eager_defaults": True}