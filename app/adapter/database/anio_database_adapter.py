import logging
import traceback
from typing import Optional, List

from app.adapter.database.models.year_model import YearModel
from sqlalchemy import select
from sqlalchemy.orm import selectinload

from app.adapter.database.models.study_plan_model import StudyPlanModel
from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.port.output.anio_repository import AnioRepository
from app.domain.entities.anio import Anio
from app.domain.factories.anio_factory import AnioFactory
from app.domain.factories.plan_de_estudio_factory import PlanDeEstudioFactory


class AnioDatabaseAdapter(AnioRepository):

    def __init__(self, session) -> None:
        self.session = session

    async def obtener_por_id(self, id_anio_plan_estudio: int) -> Optional[Anio]:
        logging.info(f'Obteniendo año por id: {id_anio_plan_estudio} en la base de datos')
        try:
            query = select(StudyPlanYearModel)\
                .where(StudyPlanYearModel.id == id_anio_plan_estudio)\
                .options(selectinload(StudyPlanYearModel.years),
                         selectinload(StudyPlanYearModel.study_plan))
            resultado = await self.session.execute(query)
            anio_plan_estudio_model: StudyPlanYearModel = resultado.scalars().first()

        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        anio_model = anio_plan_estudio_model.years
        plan_model = anio_plan_estudio_model.study_plan

        plan_de_estudio = PlanDeEstudioFactory.crear_plan_de_estudio(plan_model.id, plan_model.code, plan_model.name, plan_model.description)
        anio = AnioFactory.crear_anio_con_plan_de_estudio(anio_plan_estudio_model.id, anio_model.numeration, anio_model.name, plan_de_estudio)

        return anio

    async def obtener_por_plan_estudio_id(self, plan_estudio_id: int) -> Optional[List[Anio]]:
        try:
            query = select(StudyPlanYearModel)\
                .where(StudyPlanYearModel.study_plan_id == plan_estudio_id)\
                .options(selectinload(StudyPlanYearModel.years),
                         selectinload(StudyPlanYearModel.study_plan))
            resultado = await self.session.execute(query)
            anios: List[StudyPlanYearModel] = resultado.scalars().all()

        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        logging.info(f'Se encontraron {len(anios)} años')
        anios_domain = [ AnioFactory.crear_anio_sin_plan_de_estudio(anio.id, anio.years.numeration, anio.years.name) for anio in anios ]
        return anios_domain

