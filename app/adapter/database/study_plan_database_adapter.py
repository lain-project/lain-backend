import logging
import traceback

from sqlalchemy.orm import selectinload

from app.adapter.database.models.study_plan_model import StudyPlanModel
from app.domain.study_plan import StudyPlan
from app.application.port.output.study_plan_repository import StudyPlanRepository
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from app.adapter.database.mappers.study_plan_db_mapper import StudyPlanDBMapper

from typing import List


class StudyPlanDatabaseAdapter(StudyPlanRepository):

    def __init__(self, session: AsyncSession) -> None:
        self.session = session


    async def find_by_code(self, code: str) -> StudyPlan:
        logging.info(f'Se procede a buscar un plan de estudio {code}')
        try:
            statement = select(StudyPlanModel).where(StudyPlanModel.code == code)
            result = await self.session.execute(statement)
            study_plan: StudyPlanModel = result.scalars().first()
            return StudyPlanDBMapper.to_domain(study_plan) if study_plan else None
        except Exception:
            logging.error(f'Error al buscar el plan de estudio {code}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def find_by_id(self, id: str) -> StudyPlan:
        logging.info(f'Se procede a buscar un plan de estudio por el id {id}')
        try:
            statement = select(StudyPlanModel).where(StudyPlanModel.id == id)
            result = await self.session.execute(statement)
            study_plan: StudyPlanModel = result.scalars().first()
        except Exception:
            logging.error(f'Error al buscar el plan de estudio con id {id}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        
        if study_plan is None:
            return None

        return StudyPlanDBMapper.to_domain(study_plan)


    async def save(self, study_plan: StudyPlan, workspace_id: str) -> StudyPlan:
        logging.info(f'Se procede a guardar el plan de estudio {study_plan}')
        try:
            study_plan.workspace_id = workspace_id
            logging.info(f'Study plan a guardar en db {study_plan}')
            study_plan_model = StudyPlanModel(code=study_plan.code, name=study_plan.name, description=study_plan.description, workspace_id=study_plan.workspace_id)
            self.session.add(study_plan_model)
            await self.session.commit()
            statement = select(StudyPlanModel).where(StudyPlanModel.code == study_plan.code)
            result = await self.session.execute(statement)
            study_plan: StudyPlanModel = result.scalars().first()
            logging.info(f'Study plan guardado en db {study_plan}')
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        return StudyPlanDBMapper.to_domain(study_plan)

    async def update(self, study_plan: StudyPlan):
        logging.info(f'Se procede a actualizar el plan de estudio {study_plan}')
        try:
            async with self.session.begin_nested():
                statement = select(StudyPlanModel).where(StudyPlanModel.id == study_plan.id)
                result = await self.session.execute(statement)
                study_plan_model: StudyPlanModel = result.scalars().first()
                study_plan_model.code = study_plan.code
                study_plan_model.name = study_plan.name
                study_plan_model.description = study_plan.description
                await self.session.commit()
                logging.info(f'Se actualizo el plan de estudio {study_plan}')
        except Exception:
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            await self.session.rollback()
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def find_all(self) -> List[StudyPlan]:
        logging.info(f'Se procede a buscar los planes de estudio en la base de datos')
        statement = select(StudyPlanModel)
        result = await self.session.execute(statement)
        study_plans: List[StudyPlanModel] = result.scalars().all()
        study_plans = list(map(lambda study_plan: StudyPlanDBMapper.to_domain(study_plan), study_plans))
        logging.info(f'planes de estudio f{study_plans}')

        return study_plans