import logging
import traceback
from typing import List, Optional

from sqlalchemy import select, and_
from sqlalchemy.orm import selectinload

from app.adapter.database.mappers.study_plan_year_db_mapper import StudyPlanYearDBMapper
from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel
from app.adapter.database.mappers.year_db_mapper import YearDBMapper
from app.adapter.database.models.year_model import YearModel
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.port.output.year_repository import YearRepository
from app.domain.year import Year
from app.domain.study_plan import StudyPlan


class YearDatabaseAdapter(YearRepository):


    def __init__(self, session) -> None:
        self.session = session


    async def find_by_id(self, year_id: str) -> Optional[Year]:
        pass

    async def find_years(self, years: int) -> List[Year]:
        logging.info(f'Se prodece a buscar años en la base de datos {years}')
        try:
            statement = select(YearModel)\
                .where(YearModel.numeration <= years)\
                .options(selectinload(YearModel.study_plans_years))
            result = await self.session.execute(statement=statement)
            years: YearModel = result.scalars().all()
            if not years:
                return None
            years =  [ YearDBMapper.to_domain(year) for year in years]
            logging.info(f'Años encontrados en al base de dato en la base de datos')
            return years
        except Exception:  # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')

    async def save_all(self,study_plan: StudyPlan, years: List[Year]):
        logging.info(f'Se procede a guardar años en la base de datos')
        try:
            for year in years:
                study_plan_years = StudyPlanYearModel(
                    study_plan_id=study_plan.id,
                    year_id=year.id,
                    node_id=year.node_id
                )
                self.session.add(study_plan_years)
            await self.session.commit()
            logging.info(f'Años guardados en la base de datos')
        except Exception:  # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
        logging.error(f'Not implemented')
        return True

    async def find_by_id_and_study_plan_id(self, year_id: int, study_plan_id: int) -> Optional[Year]:
        logging.info(f'Se procede a buscar año en la base de datos por número {year_id} y plan de estudio {study_plan_id}')
        try:
            statement = select(StudyPlanYearModel)\
                .where( and_(StudyPlanYearModel.study_plan_id == study_plan_id, StudyPlanYearModel.year_id == year_id))\
                .options(selectinload(StudyPlanYearModel.years), selectinload(StudyPlanYearModel.study_plan))
            result = await self.session.execute(statement=statement)
            year: StudyPlanYearModel = result.scalars().first()
            if not year:
                return None
            logging.warning(f'Año encontrado en la base de datos { year.__dict__ }')
            year = StudyPlanYearDBMapper.to_domain_year(year)
            logging.info(f'Año encontrado en la base de datos {year}')
            return year
        except Exception:  # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')