import traceback
from datetime import datetime

from sqlalchemy import select

from app.adapter.database.models.token_model import TokenModel
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.port.output.token_repository import TokenRepository
from app.domain.token_neuralaction import TokenNeuralAction
import logging
from sqlalchemy.orm import Session


class TokenDatabaseAdapter(TokenRepository):

    def __init__(self, session: Session) -> None:
        self.session = session

    async def get_token(self) -> TokenNeuralAction:
        logging.info(f'Obtener token de la base de datos')
        statement = select(TokenModel)
        token = await self.session.execute(statement=statement)
        token = token.scalars().first()
        logging.info(f'token {token}')
        if(token is None):
            return None
        
        return token

    async def is_expired(self, token: str) -> bool:
        logging.info(f'Validar token de la base de datos')
        statement = select(TokenModel).where(TokenModel.access_token == token).limit(1)
        token = await self.session.execute(statement)
        token = token.scalars().first()
        if token is None:
            return True
        return datetime.now() > token.expires_at

    async def save_token(self, token: TokenNeuralAction) -> bool:
        logging.info(f'Se procede a guardar token en la base de datos')
        try:
            expires_at = datetime.strptime(token.expires_at, '%Y-%m-%d %H:%M:%S')
            token_model = TokenModel( access_token= token.access_token,
                                      expires_at= expires_at,
                                      user_id= token.user_id,
                                      token_type= token.token_type)
            self.session.add(token_model)
            await self.session.commit()
            return True
        except Exception as e: # pragma: no cover
            raise InternalBusinessErrorException(f"Error al guardar el token {traceback.print_exc()}")
