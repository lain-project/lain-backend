import logging

import uuid

from app.shared_kernel.domain.value_objects import UUID

from app.adapter.database.models.relacion_model import RelacionModel

from app.adapter.database.models.nodo_materia_model import NodoMateriaModel
from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel

from app.adapter.database.models.schema_model import SchemaModel

from app.adapter.database.models.study_plan_model import StudyPlanModel
from sqlalchemy import select, and_, delete

from app.domain.materia import Materia


class UtilsDatabaseAdapter:

    def __init__(self, session):
        self.session = session

    async def obtener_workspace_id(self, plan_de_estudio_id: int) -> str:
        query = select(StudyPlanModel).where(StudyPlanModel.id == plan_de_estudio_id)
        result = await self.session.execute(query)
        return result.scalars().first().workspace_id

    async def obtener_schemas_id(self, plan_de_estudio_id: int, nombre: str) -> str:
        query = select(SchemaModel).where(
            and_(SchemaModel.study_plan_id == plan_de_estudio_id, SchemaModel.name == nombre))
        result = await self.session.execute(query)
        return result.scalars().first().schema_id

    # async def agregar_nodo_id(self, model, id_nodo: str):
    #     await self.session.add(model)
    #     await self.session.commit()
    #     return model.id

    async def obtener_anio_node_id(self, id_anio) -> str:
        logging.info(f'Se busca el nodo del anio {id_anio}')
        query = select(StudyPlanYearModel).where(StudyPlanYearModel.id == id_anio)
        result = await self.session.execute(query)
        anio =  result.scalars().first()
        return anio.node_id

    async def obtener_node_id(self, modelo, identificador: str):
        query = select(modelo).where(modelo.id == identificador)
        result = await self.session.execute(query)
        return result.scalars().first().node_id

    async def guardar_nodo_materia(self, nombre: str, materia: Materia, node_id: str):
        nodo_materia = NodoMateriaModel(nombre=nombre, materia_id=str(materia.id), nodo_id=node_id)
        self.session.add(nodo_materia)
        await self.session.commit()

    async def guardar_relacion_nodos(self, id: str, nodo_inicial: str, nodo_final: str):
        relacion = RelacionModel(id=id, nodo_inicial=nodo_inicial, nodo_final=nodo_final)
        self.session.add(relacion)
        await self.session.commit()

    async def buscar_relacion_nodos(self, nodo_inicial: str, nodo_final: str) -> str:
        logging.info(f'Se busca la relacion entre {uuid.UUID(nodo_inicial)} y {nodo_final}')
        query = select(RelacionModel).where(
            and_(RelacionModel.nodo_inicial == nodo_inicial, RelacionModel.nodo_final == nodo_final))
        result = await self.session.execute(query)
        relacion: RelacionModel = result.scalars().first()
        logging.info(f'Se encontro la relacion {relacion.id}')
        return relacion.id

    async def eliminar_relacion_nodos(self, nodo_id: str):
        query = delete(RelacionModel).where(RelacionModel.id == nodo_id)
        await self.session.execute(query)