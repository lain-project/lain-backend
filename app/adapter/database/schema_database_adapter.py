import logging
import traceback
from typing import List

from app.adapter.database.models.schema_model    import SchemaModel
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.port.output.schema_repository import SchemaRepository
from app.domain.schema import Schema

class SchemaDatabaseAdapter(SchemaRepository):

    def __init__(self, session) -> None:
        self.session =  session

    async def save_all(self, schemas: List[Schema], study_plan_id: int) -> bool:
        logging.info(f'Se procede a guardar los schemas {schemas}')
        try:
            schemas = map(lambda schema: SchemaModel(id=None, name=schema.name, schema_id=schema.schema_id, study_plan_id=study_plan_id), schemas)
            self.session.add_all(schemas)
            await self.session.commit()
        except Exception: # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        return True
        