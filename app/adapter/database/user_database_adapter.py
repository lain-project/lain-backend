from datetime import datetime
import logging
import traceback
from typing import List, Optional
from app.adapter.database.mappers.user_db_mapper import UserDBMapper
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.domain.enums.user_status_enum import UserStatusEnum
from fastapi.encoders import jsonable_encoder
from sqlalchemy.ext.asyncio import AsyncSession
from app.adapter.database.models.profile_model import ProfileModel
from app.adapter.database.models.user_model import UserModel
from app.adapter.database.models.rol_model import RolModel
from app.application.port.output.user_repository import UserRepository
from app.domain.user import User
from sqlalchemy import select
from sqlalchemy.orm import selectinload



class UserDatabaseAdapter(UserRepository):

    def __init__(self, session: AsyncSession):
        self.session = session

    async def find_by_username(self, username: str) -> Optional[User]:
        logging.info(f'Se prodecede a buscar el usuario {username} en la base de datos')
        try:
            statement = select(UserModel)\
                .where(UserModel.username == username)\
                .options(selectinload(UserModel.rol), selectinload(UserModel.profile))
            result = await self.session.execute(statement=statement)
            user: UserModel = result.scalars().first()
            if not user:
                return None
            user: User  = UserDBMapper.to_domain(user)
            logging.info(f'Usuario {user.username} encontrado en la base de datos')
            return user
        except Exception: # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        

    async def find_by_email(self, email: str) -> Optional[User]:
        logging.info(f'Se prodecede a buscar el usuario por email {email} en la base de datos')
        try:
            statement = select(UserModel)\
                .where(UserModel.email == email)\
                .options(selectinload(UserModel.rol), selectinload(UserModel.profile))
            result = await self.session.execute(statement)
            user = result.scalars().first()
            if not user:
                return None
            return UserDBMapper.to_domain(user)
        except Exception: # pragma: no cover
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        

    async def create(self, user: User) -> User:
        logging.info(f'Se prodecede a crear el usuario {jsonable_encoder(user)} en la base de datos')
        try:
            rol  = await self.session.execute(select(RolModel).where(RolModel.id == user.rol.id))
            rol = rol.scalars().first()
            profile = user.profile
            profile_model = ProfileModel(lastname=profile.lastname, firstname=profile.firstname, position=profile.position)
            user_model = UserModel(username=user.username, email=user.email, password=user.password, rol=rol, created_at=datetime.now(),updated_at=datetime.now(), profile=profile_model, status=UserStatusEnum.ACTIVE.value)
            self.session.add(user_model)
            await self.session.commit()
            statement = select(UserModel).where(UserModel.username == user.username).options(selectinload(UserModel.rol), selectinload(UserModel.profile))
            user = await self.session.execute(statement)
            user = user.scalars().first()
            logging.error(f' usuario {user} creado')
        except Exception: # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        return UserDBMapper.to_domain(user)
    
    async def get_all(self) -> List[User]:
        logging.info(f'Se prodecede a obtener todos los usuarios')
        try:
            statement = select(UserModel).options(selectinload(UserModel.rol), selectinload(UserModel.profile))
            result = await self.session.execute(statement=statement)
            users = result.scalars().all()
            logging.info(f'users {users}')
            return [UserDBMapper.to_domain(user) for user in users]
        except Exception: # pragma: no cover
            logging.error(f'Error en la base de datos: {traceback.format_exc()}')
            raise InternalBusinessErrorException(f'Error en la base de datos: {traceback.format_exc()}')
        return users