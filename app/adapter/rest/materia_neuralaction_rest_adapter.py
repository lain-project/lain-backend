import asyncio
import logging

from sqlalchemy import select, join
from sqlalchemy.orm import selectinload

from app.adapter.database.models.study_plan_year_model import StudyPlanYearModel

from app.tests.utils.coroutines import async_return

from app.adapter.rest.models.generic_schema import GenericSchema
from app.adapter.rest.models.relacion_schema import RelacionSchema
from app.adapter.database.models.subject_model import SubjectModel

from app.adapter.rest.models.materia_schema import MateriaSchema

from app.adapter.database.utils_database_adapter import UtilsDatabaseAdapter
from app.application.port.output.materia_neuralactions_repository import MateriaNeuralactionsRepository
from app.domain.entities.materia import Materia
from app.config.config import settings


class MateriaNeuralActionRestAdapter(MateriaNeuralactionsRepository):

    def __init__(self, client, session):
        self.client = client
        self.session = session
        self.headers = {}
        self.utils_database = UtilsDatabaseAdapter(session)

    def establecer_headers(self, token):
        self.headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    async def guardar_materia(self, materia: Materia) -> Materia:
        workspace_id = await self.utils_database.obtener_workspace_id(materia.id_plan_de_estudio)

        anio_id, materia_id, equipo_trabajo_id, trabajos_practicos_id, unidades_id, bibliografias_id = await self._guardado_paralelo(
            materia, workspace_id)
        relacion_anio_id = await self._relacionar_nodos(materia_id, anio_id, 'Pertenece a')

        await self._crear_materia_db(materia, materia_id),
        await self.utils_database.guardar_nodo_materia('Equipo de trabajo', materia, equipo_trabajo_id),
        await self.utils_database.guardar_nodo_materia('Trabajos practicos', materia, trabajos_practicos_id),
        await self.utils_database.guardar_nodo_materia('Unidades', materia, unidades_id),
        await self.utils_database.guardar_nodo_materia('Bibliografia', materia, bibliografias_id),

        tasks = [
            self._relacionar_nodos(materia_id, equipo_trabajo_id, 'Es dictado por'),
            self._relacionar_nodos(trabajos_practicos_id, materia_id, 'Contiene'),
            self._relacionar_nodos(unidades_id, materia_id, 'Contiene'),
            self._relacionar_nodos(bibliografias_id, materia_id, 'Tiene'),
        ]
        await asyncio.gather(*tasks)

        await self.utils_database.guardar_relacion_nodos(relacion_anio_id, materia_id, anio_id),


    async def _guardado_paralelo(self, materia: Materia, workspace_id: str):
        tasks = [
            self.utils_database.obtener_anio_node_id(materia.anio.id),
            self._guardar_nodo_materia_neuralactions(materia, workspace_id),
            self._guardar_nodo_neuralactions(materia, workspace_id, 'Equipo de trabajo'),
            self._guardar_nodo_neuralactions(materia, workspace_id, 'Trabajos practicos'),
            self._guardar_nodo_neuralactions(materia, workspace_id, 'Unidades'),
            self._guardar_nodo_neuralactions(materia, workspace_id, 'Bibliografias')
        ]
        return await asyncio.gather(*tasks)

    async def _guardar_nodo_materia_neuralactions(self, materia: Materia, workspace_id: str) -> str:
        schema_materia_id = await self.utils_database.obtener_schemas_id(materia.id_plan_de_estudio, 'materia')
        schema_materia_schema = MateriaSchema(materia, workspace_id, schema_materia_id).guardar_schema()
        logging.info(f'Se procede a guardar materia {schema_materia_schema}')
        response = await self.client.post(url=settings.URL_NODE, json=schema_materia_schema, headers=self.headers)
        response = await response.json()
        logging.info(f'Se guardo materia exitosamente {response}')
        return response['id']

    async def _guardar_nodo_neuralactions(self, materia: Materia, workspace_id: str, nombre: str) -> str:
        schema_equipo_trabajo_id = await self.utils_database.obtener_schemas_id(materia.id_plan_de_estudio, 'nodo')
        schema_equipo_trabajo_schema = GenericSchema(nombre, schema_equipo_trabajo_id, workspace_id).getSchema
        logging.info(f'Se procede a guardar equipo trabajo {schema_equipo_trabajo_schema}')
        response = await self.client.post(url=settings.URL_NODE, json=schema_equipo_trabajo_schema,
                                          headers=self.headers)
        response = await response.json()
        return response['id']

    async def _crear_materia_db(self, materia: Materia, node_id: str):
        materia_db = SubjectModel(id=str(materia.id), node_id=node_id)
        self.session.add(materia_db)
        await self.session.commit()

    async def _relacionar_nodos(self, origen_id: str, destino_id: str, relacion_nombre: str):
        schema_relacion = RelacionSchema(origen_id, destino_id)
        schema_relacion.establecer_nombre(relacion_nombre)
        schema_relacion = schema_relacion.get_schema

        logging.info(f'Se procede a crear relacion {schema_relacion}')
        response = await self.client.post(url=settings.URL_RELATIONSHIP, json=schema_relacion, headers=self.headers)
        response = await response.json()
        return response['id']

    async def actualizar_materia(self, materia: Materia) -> Materia:
        materia_node_id = await self.utils_database.obtener_node_id(SubjectModel, str(materia.id))
        anio_anterior = await self._obtener_anio_actual(materia)
        await self._eliminar_relacion_anio(materia_node_id, anio_anterior)

        anio_actual = await self.utils_database.obtener_anio_node_id(materia.anio.id)
        relacion_anio_id = await self._relacionar_nodos(materia_node_id, anio_actual, 'Pertenece a')

        await self.utils_database.guardar_relacion_nodos(relacion_anio_id, materia_node_id, anio_actual),

        return await self._actualizar_materia_neuralactions(materia, materia_node_id)

    async def _obtener_anio_actual(self, materia: Materia):
        statement = select(StudyPlanYearModel)\
            .join(SubjectModel, StudyPlanYearModel.id == SubjectModel.study_plan_year_id)\
            .where(SubjectModel.id == str(materia.id))
        anio = await self.session.execute(statement)
        anio: StudyPlanYearModel = anio.scalars().first()
        return anio.node_id

    async def _eliminar_relacion_anio(self, materia_nodo_id: str, anio_nodo_id: str):
        relacion_anio_id = await self.utils_database.buscar_relacion_nodos(materia_nodo_id, anio_nodo_id)
        relacion_schema: dict = {"id": str(relacion_anio_id)}
        logging.info(f'Se procede a eliminar relacion {relacion_schema}')
        await self.client.delete(url=f'{settings.URL_RELATIONSHIP}', json=relacion_schema, headers=self.headers)
        await self.utils_database.eliminar_relacion_nodos(relacion_anio_id)

    async def _actualizar_materia_neuralactions(self, materia: Materia, node_id):
        schema_materia = MateriaSchema(materia)
        schema_materia.establecer_id(node_id)
        schema_materia_schema = schema_materia.actualizar_schema()
        logging.info(f'Se procede a actualizar materia {schema_materia_schema}')
        response = await self.client.put(url=settings.URL_NODE, json=schema_materia_schema, headers=self.headers)
        response = await response.json()
        logging.error(f'Se actualizo materia exitosamente {response}')
        return response