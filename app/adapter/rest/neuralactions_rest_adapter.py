import traceback
from typing import List
import asyncio
import aiohttp
import requests
import logging

from app.adapter.rest.models.year_schema import YearSchema

from app.adapter.rest.mappers.year_mapper import YearMapper
from app.adapter.rest.models.schema_rest import SchemaRest
from app.adapter.rest.mappers.schema_mapper import SchemaMapper
from app.adapter.rest.models.token_model_rest import TokenModelRest
from app.config.config import settings
from app.application.port.output.neuralactions_repository import NeuralactionsRepository
from app.domain.schema import Schema
from app.domain.token_neuralaction import TokenNeuralAction

# TODO: Planificar testing
from app.domain.materia import Materia
from app.domain.year import Year


class NeuralactionsRestAdapter(NeuralactionsRepository):

    def __init__(self, session):
        self.username = settings.USER_NA
        self.password = settings.PASS_NA
        self.headers = {}
        self.session = session
    

    def set_headers(self, token):
        self.headers = { 'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    async def authenticate(self) -> TokenNeuralAction:
        logging.info(
            f'Se procede a generar token en Neuralactions url {settings.URL_LOGIN} - {self.username} {self.password}')
        try:
            body = {'email': self.username, 'password': self.password}
            response = await self.session.post(url=settings.URL_LOGIN, json=body)
            response = await response.json()
            token = TokenModelRest(**response)
            logging.info(f'Se genero autenticacion en Neuralactions exitosamente')
            return token.to_domain()
        except Exception:
            logging.error(f'Error al generar token en Neuralactions {traceback.print_exc()}')
            raise Exception(f'Error al generar token en Neuralactions. {traceback.print_exc()}')
    

    async def save_workspace(self, workspace_name: str, workspace_description: str) -> str:
        logging.info(
            f'Se procede a crear workspace en Neuralactions url {settings.URL_WORKSPACE}')
        try:
            body = {'name': workspace_name, 'description': workspace_description}
            logging.info(f'se logea body {body}')
            logging.info(f' se logeaa headers {self.headers}')
            logging.info(f' se logeaa url {settings.URL_WORKSPACE}')
            response = await self.session.post(url=settings.URL_WORKSPACE, json=body, headers=self.headers)
            response = await response.json()
            return response['id']
        except Exception as e:
            logging.error(f'Error al crear workspace en Neuralactions')
            raise Exception(
                'Error al crear workspace en Neuralactions. ' + str(e))

    async def create_schemas(self, workspace_id: str) -> List[Schema]:
        logging.info(
            f'Se procede a crear schema en Neuralactions url {settings.URL_SCHEMA}')
        headers = self.headers
        schema = SchemaRest(workspace_id)
        try:
            schemas = [
                schema.anio,
                schema.bibliografia,
                schema.contenido_minimo,
                schema.descriptor,
                schema.docente,
                schema.materia,
                schema.programa,
                schema.tema,
                schema.nodo,
                schema.trabajo_practico,
                schema.unidad
            ]
            tasks = []
            for schem in schemas:
                tasks.append(self._masive_create_schema(session=self.session, data=schem, headers=headers))
            schemes = await asyncio.gather(*tasks, return_exceptions=True)
            logging.info(f' Schemas creados')
            logging.info(f' {schemes}')
            return schemes
        except Exception as e:
            logging.error(f'Error al crear schemas en Neuralactions {traceback.print_exc()}')
            raise Exception(
                'Error al crear schemas en Neuralactions. ' + str(e))

        
    async def _masive_create_schema(self, session: aiohttp.ClientSession, data, headers):
        resp = await session.post(url=settings.URL_SCHEMA, data=data, headers=headers)
        data = await resp.json()
        logging.info('Se creo schema en neuralactions exitosamente')
        return SchemaMapper.to_domain(data)

    def _create_schema(self, data: str, headers) -> Schema:
        logging.info(
            f'Se procede a crear schema en Neuralactions url {settings.URL_SCHEMA}')
        try:
            response = requests.post(url=settings.URL_SCHEMA, json=data, headers=headers)
            logging.info(f'Schema {response.json()["name"]} creado exitosamente')
            return SchemaMapper.to_domain(response.json())
        except Exception as e:
            logging.error(f'response {response}')
            logging.error(f'Error al crear schema en Neuralactions {str(e)}')
            raise Exception(
                'Error al crear schema en Neuralactions. ' + str(e))

    async def update_workspace(self, workspace_id: str, workspace_name: str, workspace_description: str) -> bool:
        logging.info(f'Se procede a actualizar workspace en Neuralactions url {settings.URL_WORKSPACE}')
        try:
            body = {'id': workspace_id, 'name': workspace_name, 'description': workspace_description}
            response = await self.session.put(url=settings.URL_WORKSPACE, json=body, headers=self.headers)
            response = await response.json()
            logging.info(f'Workspace {workspace_name} actualizado exitosamente')
            return response is not None
        except Exception as e:
            logging.error(f'Error al actualizar workspace en Neuralactions {traceback.print_exc()}')
            raise Exception(
                'Error al actualizar workspace en Neuralactions. ' + str(e))



    async def save_years(self, workspace_id, years: List[Year]) -> List[Year]:
        logging.info(f'Se procede a crear años en neuralactions para el workspace {workspace_id}')
        try:
            tasks = []
            for year in years:
                year.workspace_id = workspace_id
                year_schema = YearMapper.to_schema(year)
                tasks.append(self._masive_create_years(session=self.session, year=year_schema))
            years = await asyncio.gather(*tasks, return_exceptions=True)
            logging.info(f'Años creados exitosamente {years}')
            return years
        except Exception as e:
            logging.error(f'Error al actualizar workspace en Neuralactions {traceback.print_exc()}')
            raise Exception(
                'Error al actualizar workspace en Neuralactions. ' + str(e))

    async def _masive_create_years(self, session: aiohttp.ClientSession, year: YearSchema):
        logging.warning(f'Massive create year {year}')
        resp = await session.post(url=settings.URL_NODE, data=year.serialize(), headers=self.headers)
        data = await resp.json()
        year.node_id = data['id']
        logging.info(f'Se creo año {data} en neuralactions exitosamente')
        return year

    async def save_subject(self, subject: Materia) -> Materia:
        logging.info(f'Se procede a crear materia en neuralactions para el workspace {subject}')
        pass