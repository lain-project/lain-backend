import logging
import traceback

from app.adapter.rest.models.token_model_rest import TokenModelRest

from app.application.port.output.token_repository import TokenRepository

from app.application.port.output.autenticacion_neuralactions_repository \
    import AutenticacionNeuralactionsRepository
from app.config.config import settings


class AutenticacionNeuralactionsRestAdapter(AutenticacionNeuralactionsRepository):

    def __init__(self, session, token_repository: TokenRepository):
        self.token_repository = token_repository
        self.username = settings.USER_NA
        self.password = settings.PASS_NA

    async def autenticarse(self) -> str:
        token_neuralaction = await self.token_repository.get_token()
        if token_neuralaction is None:
            return await self._refrescar_token()
        if await self.token_repository.is_expired(token_neuralaction.access_token):
            return await self._refrescar_token()
        return token_neuralaction.access_token

    async def _refrescar_token(self):
        logging.info(f'Se procede a logear en neuralactions')
        token_neuralaction = await self._obtener_token_neuralactions()
        await self.token_repository.save_token(token_neuralaction)
        logging.info(f'Autenticado en neuralactions')
        return token_neuralaction.access_token

    async def _obtener_token_neuralactions(self) -> str:
        logging.info(
            f'Se procede a generar token en Neuralactions url {settings.URL_LOGIN} - {self.username} {self.password}')
        try:
            body = {'email': self.username, 'password': self.password}
            response = await self.session.post(url=settings.URL_LOGIN, json=body)
            response = await response.json()
            token = TokenModelRest(**response)
            logging.info(f'Se genero autenticacion en Neuralactions exitosamente')
            return token.to_domain()
        except Exception:
            logging.error(f'Error al generar token en Neuralactions {traceback.print_exc()}')
            raise Exception(f'Error al generar token en Neuralactions. {traceback.print_exc()}')

