from dataclasses import dataclass
from app.domain.token_neuralaction import TokenNeuralAction


@dataclass
class TokenModelRest:
    access_token: str
    token_type: str
    user_id: int
    expires_at: str

    def to_domain(self): # pragma: no cover
        return TokenNeuralAction(
            access_token=self.access_token,
            token_type=self.token_type,
            user_id=self.user_id,
            expires_at=self.expires_at
        )