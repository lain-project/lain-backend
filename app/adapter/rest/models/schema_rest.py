import json


class SchemaRest:

    def __init__(self, workspace_id):
        self.workspace_id = workspace_id

    @property
    def materia(self) -> str:
        return json.dumps({"name": "materia",
                           "fields": [
                               {
                                   "name": "imagen",
                                   "hard_type": "picture",
                                   "pos": "0"
                               },
                               {
                                   "name": "nombre",
                                   "hard_type": "string",
                                   "pos": "1"
                               },
                               {
                                   "hard_type": "string",
                                   "pos": "2",
                                   "name": "codigo"
                               },
                               {
                                   "hard_type": "string",
                                   "pos": "3",
                                   "name": "regimen"
                               },
                               {
                                   "hard_type": "string",
                                   "pos": "4",
                                   "name": "horas_semanales"
                               },
                               {
                                   "hard_type": "string",
                                   "pos": "5",
                                   "name": "total_horas"
                               }
                           ],
                           "workspaces": {"active_workspace_id": self.workspace_id}
                           })

    @property
    def anio(self) -> str:
        return json.dumps({
            "name": "año",
            "fields": [
                {
                    "name": "numero",
                    "hard_type": "string",
                    "pos": "0"
                },
                {
                    "name": "nombre",
                    "hard_type": "string",
                    "pos": "1"
                }
            ],
            "workspaces": {
                "active_workspace_id": self.workspace_id
            }
        })

    @property
    def nodo(self) -> str:
        return json.dumps({"name": "nodo", "fields": [{"name": "nombre", "hard_type": "string", "pos": "0"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def docente(self) -> str:
        return json.dumps({"name": "docente", "fields": [{"name": "imagen", "hard_type": "picture", "pos": "0"}, {"name": "nombres", "hard_type": "string", "pos": "1"}, {"name": "apellidos", "hard_type": "string", "pos": "2"}, {"name": "email", "hard_type": "string", "pos": "3"}, {"name": "cargo", "hard_type": "string", "pos": "4"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def programa(self) -> str:
        return json.dumps({"name": "programa", "fields": [{"name": "contenido_minimo", "hard_type": "html", "pos": "0"}, {"name": "fundamentos", "hard_type": "html", "pos": "1"}, {"name": "objetivos", "hard_type": "html", "pos": "2"}, {"name": "metodologia", "hard_type": "html", "pos": "3"}, {"name": "evaluacion", "hard_type": "html", "pos": "4"}, {"name": "bibliografia_general", "hard_type": "html", "pos": "5"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def bibliografia(self) -> str:
        return json.dumps({"name": "bibliografia", "fields": [{"name": "imagen", "hard_type": "picture", "pos": "0"}, {"name": "nombre", "hard_type": "string", "pos": "1"}, {"name": "isbn", "hard_type": "string", "pos": "2"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def unidad(self) -> str:
        return json.dumps({"name": "unidad", "fields": [{"name": "numero", "hard_type": "string", "pos": "0"}, {"name": "nombre", "hard_type": "string", "pos": "1"}, {"name": "objetivos", "hard_type": "html", "pos": "2"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def trabajo_practico(self) -> str:
        return json.dumps({"name": "trabajo practico", "fields": [{"name": "numero", "hard_type": "string", "pos": "0"}, {"name": "nombre", "hard_type": "string", "pos": "1"}, {"name": "actividad", "hard_type": "html", "pos": "2"}, {"name": "materiales", "hard_type": "html", "pos": "3"}, {"name": "ambito_de_practica", "hard_type": "html", "pos": "4"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def contenido_minimo(self) -> str:
        return json.dumps({"name": "contenido minimo", "fields": [{"name": "unidad", "hard_type": "string", "pos": "0"}, {"name": "nombre", "hard_type": "string", "pos": "1"}, {"name": "descripcion", "hard_type": "html", "pos": "2"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def descriptor(self) -> str:
        return json.dumps({"name": "descriptor", "fields": [{"name": "nombre", "hard_type": "string", "pos": "0"}, {"name": "descripcion", "hard_type": "html", "pos": "1"}], "workspaces": {"active_workspace_id": self.workspace_id}})

    @property
    def tema(self) -> str:
        return json.dumps({"name": "tema", "fields": [{"name": "nombre", "hard_type": "string", "pos": "0"}, {"name": "descripcion", "hard_type": "html", "pos": "1"}], "workspaces": {"active_workspace_id": self.workspace_id}})
