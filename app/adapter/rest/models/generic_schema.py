class GenericSchema:

    def __init__(self, nombre: str, schema_id: str, workspace_id: str) -> None:
        self.nombre = nombre
        self.schema_id = schema_id
        self.workspace_id = workspace_id


    @property
    def getSchema(self) -> dict:
        return {
            "values": {
                "name": self.nombre,
                "nombre": self.nombre,
                "scheme_id": self.schema_id
            },
            "workspaces": {
                "active_workspace_id": self.workspace_id
            }
        }