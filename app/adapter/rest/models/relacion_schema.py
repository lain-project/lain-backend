class RelacionSchema:

    def __init__(self, nodo_inicial: str, nodo_final: str):
        self.nodo_inicial = nodo_inicial
        self.nodo_final = nodo_final
        self.nombre = ""

    def establecer_nombre(self, nombre: str):
        self.nombre = nombre


    @property
    def get_schema(self) -> dict:
        return {
            "node_a": self.nodo_inicial,
            "node_b": self.nodo_final,
            "relation_type": self.nombre
        }

