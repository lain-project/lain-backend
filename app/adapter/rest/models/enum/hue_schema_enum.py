from enum import Enum

class HueSchemaEnum(Enum):
    """
    Enum for color schema.
    """
    YEAR = 257
