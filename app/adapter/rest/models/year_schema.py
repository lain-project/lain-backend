import json
from dataclasses import dataclass

from app.adapter.rest.models.enum.hue_schema_enum import HueSchemaEnum


@dataclass
class YearSchema:
    number: int = None
    name: str = None
    schema_id: str = None
    workspace_id: str = None
    hue: HueSchemaEnum = None
    node_id: str = None


    def serialize(self):
        return json.dumps({
            "values": {
                "name": self.name,
                "nombre": self.name,
                "numero": self.number,
                "scheme_id": self.schema_id,
                "hue": HueSchemaEnum.YEAR.value,
            },
            "workspaces": {
                "active_workspace_id": self.workspace_id
            }
        })