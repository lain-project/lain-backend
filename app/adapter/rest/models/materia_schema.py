from app.domain.entities.materia import Materia


class MateriaSchema:

    def __init__(self, materia: Materia, workspace_id: str = None, schema_id: str = None) -> None:
        self.id = None
        self.name = materia.obtener_nombre()
        self.codigo = materia.obtener_codigo()
        self.regimen = materia.obtener_regimen()
        self.horas_semanales = materia.horas_semanales
        self.horas_totales = materia.horas_totales
        self.workspace_id = workspace_id
        self.schema_id = schema_id
    
    def establecer_id(self, id: str) -> None:
        self.id = id

    def guardar_schema(self) -> dict:
        return {
            "values": {
                "name": self.name,
                "nombre": self.name,
                "codigo": self.codigo,
                "regimen": self.regimen,
                "horas_semanales": str(self.horas_semanales),
                "total_horas": str(self.horas_totales),
                "scheme_id": self.schema_id
            },
            "workspaces": {
                "active_workspace_id": self.workspace_id
            }
        }
    
    def actualizar_schema(self) -> dict:
        return {
            "id": str(self.id),
            "values": {
                "name": self.name,
                "nombre": self.name,
                "codigo": self.codigo,
                "regimen": self.regimen,
                "horas_semanales": str(self.horas_semanales),
                "total_horas": str(self.horas_totales),
            },
        }