class AnioSchema:

    def __init__(self, workspace_id: str) -> None:
        self.name = 'anio'
        self.fields = [
            {
                "name": "numero",
                "hard_type": "string",
                "pos": "0"
            },
            {
                "name": "nombre",
                "hard_type": "string",
                "pos": "1"
            }
        ]
        self.workspace_id = workspace_id

    @property
    def getSchema(self) -> dict:
        return {
            "name": self.name,
            "fields": self.fields,
            "workspaces": {
                "active_workspace_id": self.workspace_id
            }
        }
