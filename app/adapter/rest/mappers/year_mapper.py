from app.adapter.rest.models.enum.hue_schema_enum import HueSchemaEnum
from app.adapter.rest.models.year_schema import YearSchema
from app.domain.year import Year


class YearMapper:

    @staticmethod
    def to_schema(year: Year) -> YearSchema:
        return YearSchema(
            number= year.numeration,
            name= year.name,
            schema_id= year.schema_id,
            hue=HueSchemaEnum.YEAR.value,
            workspace_id=year.workspace_id
        )

    @staticmethod
    def to_domain(year_schema: YearSchema) -> Year:
        return Year(
            name= year_schema.name,
            schema_id= year_schema.schema_id,
        )