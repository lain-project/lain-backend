import logging
from app.domain.schema import Schema


class SchemaMapper:

    @staticmethod
    def to_domain(schema_rest: dict) -> Schema:
        logging.error(f'mapper: {schema_rest}')
        return Schema(
            id=None,
            name=schema_rest['name'],
            description="",
            schema_id=schema_rest['id'],
            study_plan_id=None
        )