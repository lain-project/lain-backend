import logging
from app.application.exception.bad_request_business_exception import BadRequestBusinessErrorException
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from google.oauth2 import id_token
from google.auth.transport import requests
from google.auth.exceptions import GoogleAuthError, UserAccessTokenError 
from app.config.config import settings

from app.application.port.output.google_repository import GoogleRepository


class GoogleSecurityAdapter(GoogleRepository):


    def google_verify(self, token_id: str) -> str: # pragma: no cover
        logging.info(f'Se procede a verificar el token en Google')
        try:
            idinfo = id_token.verify_oauth2_token(token_id, requests.Request(), settings.GOOGLE_CLIENT_ID)
            logging.error(f'El token es valido')
            logging.error(f'{idinfo}')
            return idinfo['email']
        except UserAccessTokenError:
            raise BadRequestBusinessErrorException(f'El token no es valido')
        except GoogleAuthError:
            logging.error(f'error: {str(GoogleAuthError)}')
            raise InternalBusinessErrorException(f'Error en la autenticacion de Google')