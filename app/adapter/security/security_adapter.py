from app.application.exception.business_error_exception import BusinessErrorException
from app.application.port.output.security_repository import SecurityRepository
from app.domain.user_token import UserToken
from passlib.context import CryptContext
from jwt import encode, decode
from jwt.exceptions import DecodeError, ExpiredSignatureError
from fastapi.encoders import jsonable_encoder
from app.config.config import settings
class SecurityAdapter(SecurityRepository):
    def __init__(self):
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.algorithm = "HS256"

    def get_algorithm(self) -> str:
        return self.algorithm

    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        try:
            return self.pwd_context.verify(plain_password, hashed_password)
        except Exception: # pragma: no cover 
            return False

    def encode(self, user: UserToken, expire: int) -> str:
            return encode(
                payload={"exp": expire, "user": jsonable_encoder(user)},
                key="secret",
                algorithm=self.algorithm,
            )


    def decode(self, token: str) -> UserToken:
        try:
            return decode(token, key="secret", algorithms=[self.algorithm]).get("user")
        except DecodeError:
            raise BusinessErrorException(f"Error al decodificar token")
        except ExpiredSignatureError:
            raise BusinessErrorException(f"El token ha expirado")

    def get_hashed_password(self, plain_password: str) -> str:
        return self.pwd_context.hash(plain_password)

