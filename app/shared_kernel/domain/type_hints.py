from typing import List
from app.shared_kernel.domain.events import DomainEvent

DomainEvents = List[DomainEvent]
