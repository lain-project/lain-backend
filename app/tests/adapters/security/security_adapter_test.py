from app.adapter.security.security_adapter import SecurityAdapter
from app.application.exception.business_error_exception import BusinessErrorException
from app.domain.user_token import UserToken
from pytest import mark, raises

@mark.integration
def test_get_algorithm_successfull():
    algorithm = SecurityAdapter().get_algorithm()

    assert algorithm == 'HS256'


@mark.integration
def test_verify_password_successfull():
    hashed_password = '$2b$12$IhMmgAgnQ7CG9cGn3VTX5eAdqVrGTgEpewG5dSvliYxohcwIgv5vK'
    result = SecurityAdapter().verify_password('admin', hashed_password)

    assert result

@mark.integration
def test_verify_password_failure():
    hashed_password = '$2b$12$IhMmgAgnQ7CG9cGn3VTX5eAdqVrGTgEpewG5dSvliYxohcwIgv5vK'
    result = SecurityAdapter().verify_password('admin1', hashed_password)

    assert not result


@mark.integration
def test_encode_successfull():
    user_token = UserToken('himura', 'himura@test', 'image', 'ADMIN', False)
    encode = SecurityAdapter().encode(user_token, 11000000000)
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjExMDAwMDAwMDAwLCJ1c2VyIjp7InVzZXJuYW1lIjoiaGltdXJhIiwiZW1haWwiOiJoaW11cmFAdGVzdCIsImF2YXRhciI6ImltYWdlIiwicm9sbmFtZSI6IkFETUlOIiwiZ29vZ2xlIjpmYWxzZX19.R2oRA8wc-wPkdZEhJU-FaDeUT67rZjXN23O62OBUNjg'

    assert token == encode 

@mark.integration
def test_decode_successfull():
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjExMDAwMDAwMDAwLCJ1c2VyIjp7InVzZXJuYW1lIjoiaGltdXJhIiwiZW1haWwiOiJoaW11cmFAdGVzdCIsImF2YXRhciI6ImltYWdlIiwicm9sbmFtZSI6IkFETUlOIiwiZ29vZ2xlIjpmYWxzZX19.R2oRA8wc-wPkdZEhJU-FaDeUT67rZjXN23O62OBUNjg'
    user = UserToken(**SecurityAdapter().decode(token))

    assert user.username == 'himura'
    assert user.email == 'himura@test'
    assert user.avatar == 'image'


@mark.integration
def test_decode_token_invalid_failure():
    token = 'eyJ0eXAiOiJKV1QaLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjExMDAwMDAwMDAwLCJ1c2VyIjp7InVzZXJuYW1lIjoiaGltdXJhIiwiZW1haWwiOiJoaW11cmFAdGVzdCIsImF2YXRhciI6ImltYWdlIiwicm9sbmFtZSI6IkFETUlOIiwiZ29vZ2xlIjpmYWxzZX19.R2oRA8wc-wPkdZEhJU-FaDeUT67rZjXN23O62OBUNjg'
    message = 'Error al decodificar token'

    with raises(BusinessErrorException, match=message):
        assert SecurityAdapter().decode(token)


@mark.integration
def test_decode_token_expired_failure():
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoiaGltdXJhIiwiZW1haWwiOiJoaW11cmFAdGVzdCIsImF2YXRhciI6ImltYWdlIiwicm9sbmFtZSI6IkFETUlOIiwiZ29vZ2xlIjpmYWxzZX0sImV4cCI6MTAwMDAwMDB9.KnLgDVdiDD4APmr9jUm7d7vsk1vhJVdMVNFR8P366SQ'
    message = 'El token ha expirado'

    with raises(BusinessErrorException, match=message):
        assert SecurityAdapter().decode(token)
    
@mark.integration
def test_when_hashed_password():
    password = 'password'
    hash_password = SecurityAdapter().get_hashed_password(password)

    assert password is not hash_password