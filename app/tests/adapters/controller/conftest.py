from fastapi.testclient import TestClient
from pytest import fixture
from typing import Generator
from app.main import app
from app.adapter.controller.dependencies.deps import is_admin



@fixture(scope="module")
def client() -> Generator:
    app.dependency_overrides[is_admin] = lambda: False
    with TestClient(app) as c:
        yield c