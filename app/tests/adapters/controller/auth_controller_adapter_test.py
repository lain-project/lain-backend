from pytest import mark
from unittest.mock import patch

from app.adapter.controller.model.token_rest import TokenRest
from app.adapter.controller.model.user_rest import UserRest
from app.config.config import settings
from app.domain.token import Token
from fastapi.testclient import TestClient

url = f'{settings.ROUTE}/auth'


@mark.integration
@mark.asyncio
@patch("app.application.usecase.login_use_case.LoginUseCase.execute")
async def test_when_login_is_called_then_return_201(usecase, client: TestClient):
    expected = TokenRest(access_token='access_token', token_neuralaction='token_neuralaction')
    token = Token(access_token="access_token", token_neuralaction="token_neuralaction")
    user = UserRest(username="username", password="password")
    usecase.return_value = token
    response = client.post(f'{url}/login', json=user.dict())
    assert response.status_code == 201
    assert response.json() == expected.__dict__


@mark.integration
@mark.asyncio
@patch("app.application.usecase.google_login_use_case.GoogleLoginUseCase.execute")
async def test_when_google_login_is_called_then_return_201(usecase, client: TestClient):
    expected = TokenRest(access_token="access_token", token_neuralaction="token_neuralaction")
    token = Token(access_token="access_token", token_neuralaction="token_neuralaction")
    usecase.return_value = token
    response = client.post(f'{url}/google?token_id=token', json={})
    assert response.status_code == 201
    assert response.json() == expected.__dict__