from app.adapter.controller.model.user_rest import UserCreateRest
from app.domain.profile import Profile
from app.domain.rol import Rol
from app.domain.user import User
from fastapi.testclient import TestClient
from pytest import mark
from unittest.mock import patch
from app.config.config import settings


url = f'{settings.ROUTE}/users'
token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoiaGltdXJhIiwiZW1haWwiOiJoaW11cmFAdGVzdCIsImF2YXRhciI6ImltYWdlIiwicm9sbmFtZSI6IkFETUlOIiwiZ29vZ2xlIjpmYWxzZX0sImV4cCI6MTAwMDAwMDAwMDAwMH0.1UJ4aK5zcBYzHaqtsxzzQX8b71mr4XbmP-yezBsqQg0"

pytestmark = mark.asyncio

@mark.integration
@patch("app.application.usecase.create_user_use_case.CreateUserUseCase.execute")
async def test_when_create_user_is_called_then_return_201(usecase, client: TestClient):
    user = UserCreateRest(username="username", password="password", email="email", firstname="firstname", lastname="lastname", 
        position="Titular", 
        rol_id=1, 
        status=1, 
        avatar="avatar")
    user_domain = User(username="username", password="password", email="email", rol=Rol(1, "ADMIN"))
    usecase.return_value = user_domain

    response = client.post(f'{url}', json=user.dict(), headers={"Authorization": token})

    assert response.status_code == 201
    assert response


@mark.integration
@patch("app.application.usecase.get_users_use_case.GetUsersUseCase.execute")
async def test_when_get_users_is_called_then_return_200(usecase, client: TestClient):

    profile = Profile(firstname="firstname", lastname="lastname", position="Titular")
    rol = Rol(1, "ADMIN")
    user_domain = User(id=1, status=1, avatar='image',username="username", password="password", email="email", rol=rol, profile=profile)

    usecase.return_value = [user_domain]

    response = client.get(f'{url}', headers={"Authorization": token})

    assert response.status_code == 200