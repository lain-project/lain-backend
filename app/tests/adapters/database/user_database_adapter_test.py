# from app.adapter.database.user_database_adapter import UserDatabaseAdapter
# from app.adapter.database.database import ProfileModel, RolModel, UserModel
# from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
# from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
# from app.domain.enums.profile_position_enum import ProfilePositionEnum
# from app.domain.profile import Profile
# from app.domain.rol import Rol
# from app.domain.user import User
# from pytest import fixture, mark
# from pony.orm import db_session, flush
#
#
# @db_session()
# def clean_db():
#     UserModel.select().delete(bulk=True)
#     ProfileModel.select().delete(bulk=True)
#
# @fixture(scope="function")
# @db_session()
# def setUp():
#     clean_db()
#     flush()
#     profile = ProfileModel(id=1, firstname='test', lastname='test', position=ProfilePositionEnum.TITULAR.value)
#     UserModel(id=1,username='himura-test', email='himura@test', password='test', rol=1, profile=profile.id)
#
#
# @mark.integration
# def test_when_find_username_then_return_user(setUp):
#     user = UserDatabaseAdapter().find_by_username('himura-test')
#
#     assert user is not None
#     assert user.username == 'himura-test'
#     assert user.email == 'himura@test'
#     assert user.rol.name == 'ADMIN'
#
#
# @mark.integration
# def test_when_find_username_then_failure(setUp):
#
#     username = 'himura-test-failure'
#
#     result = UserDatabaseAdapter().find_by_username(username)
#
#     assert  result is None
#
#
# @mark.integration
# def test_when_find_email_then_return_user(setUp):
#
#     user = UserDatabaseAdapter().find_by_email('himura@test')
#
#     assert user is not None
#     assert user.username == 'himura-test'
#     assert user.email == 'himura@test'
#     assert user.rol.name == 'ADMIN'
#
#
# @mark.integration
# def test_when_find_email_then_failure(setUp):
#
#     email = 'himura@gmail'
#     result = UserDatabaseAdapter().find_by_email(email)
#
#     assert result is None
#
# @mark.integration
# def test_when_create_user_then_return_user(setUp):
#     profile = Profile(firstname='Kenji', lastname='Himura', position=ProfilePositionEnum.TITULAR.value)
#     rol = Rol(id=1, name='ADMIN')
#     user = User(id=2, username="kenji", email="kenji@test", password="test", rol=rol, profile=profile)
#     user_model = UserDatabaseAdapter().create(user)
#
#     assert user_model is not None
#     assert user_model.id == 2
#     assert user_model.username == "kenji"
#     assert user_model.email == "kenji@test"
#
# @mark.integration
# def test_when_get_all_return_users(setUp):
#     users = UserDatabaseAdapter().get_all()
#
#     assert len(users) == 1
#     assert users[0].username == 'himura-test'
#     assert users[0].email == 'himura@test'
#     assert users[0].rol.name == 'ADMIN'
