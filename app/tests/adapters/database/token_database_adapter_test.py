# from datetime import datetime
# from app.domain.token import Token
# from app.adapter.database.database import TokenModel, UserModel
# from app.adapter.database.token_database_adapter import TokenDatabaseAdapter
# from app.domain.token_neuralaction import TokenNeuralAction
# from pytest import fixture, mark
# from pony.orm import db_session, flush
#
# @fixture(scope="function")
# @db_session()
# def clean_db():
#     TokenModel.select().delete(bulk=True)
#
# @fixture(scope="module")
# @db_session()
# def setUp():
#     TokenModel.select().delete(bulk=True)
#     flush()
#     TokenModel(id=1, access_token='test', expires_at='2025-01-01', user_id=1, token_type='test')
#
# @fixture(scope="function")
# @db_session()
# def expired_token():
#     TokenModel.select().delete(bulk=True)
#     TokenModel(id=1, access_token='test', expires_at='2020-01-01', user_id=1, token_type='test')
#
# @mark.integration
# def test_when_get_token_and_exist_then_return_token(setUp):
#     date = datetime(2025, 1, 1, 12, 0)
#     token = TokenDatabaseAdapter().get_token()
#
#     assert token is not None
#     assert token.access_token == 'test'
#     assert token.expires_at == date
#     assert token.user_id == 1
#
#
# @mark.integration
# def test_when_get_token_and_not_exist_return_none(clean_db):
#     token = TokenDatabaseAdapter().get_token()
#
#     assert token is None
#
#
# @mark.integration
# def test_when_token_is_not_expired_true(expired_token):
#     is_expired = TokenDatabaseAdapter().is_expired('test')
#
#     assert is_expired
#
#
# @mark.integration
# def test_when_token_is_not_expired_false(expired_token):
#     is_expired = TokenDatabaseAdapter().is_expired('test1')
#
#     assert is_expired
#
#
# @mark.integration
# def test_when_save_token(clean_db):
#
#     token = TokenNeuralAction(access_token='test', expires_at='2025-01-01', user_id=1, token_type='test')
#     saved = TokenDatabaseAdapter().save_token(token)
#
#     assert saved
#
#
