from asyncio import Future


def async_return(value):
    future = Future()
    future.set_result(value)
    return future