import uuid

from pytest import fixture, mark
from app.domain.rol import Rol
from app.domain.user import User


@fixture(scope="function")
def rol():
    return Rol(uuid.uuid4().int, "ADMIN", True)

@mark.unit
def test_user_init(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id=id, username=username, email=email, password=password, avatar=avatar, google=google, status=status, rol=rol)

    assert user.id == id
    assert user.username == username
    assert user.email == email
    assert user.rol == rol


@mark.unit
def test_user_equals(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)
    user2 = User(id, username, email, password, avatar, google, status, rol)

    assert user == user2


@mark.unit
def test_user_not_equals(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id=id, username=username, email=email, password=password, avatar=avatar, google=google, status=status, rol=rol)
    user2 = User(id=uuid.uuid4(), username='himuraxkenji2', email='himura@mail.com', password=password, avatar=avatar, google=google, status=status, rol=rol)

    assert user != user2


@mark.unit
def test_user_same_email(rol):
    id = uuid.uuid4()
    username = "himuraxkenji"
    email = "himuraxkenji@gmail.com"
    password = "12345678"
    avatar = "https://www.myutaku.com/media/personnage/3792.jpg"
    google = False
    status = False

    user = User(id, username, email, password, avatar, google, status, rol)
    user2 = User(uuid.uuid4(), 'himuraxkenji2', email, password, avatar, google, status, rol)

    assert user == user2
