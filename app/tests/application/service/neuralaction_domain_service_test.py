from datetime import datetime
from app.application.service.neuralaction_domain_service import NeuralactionDomainService
from app.domain.token_neuralaction import TokenNeuralAction
from pytest import fixture, mark
from unittest.mock import Mock
from app.tests.utils.coroutines import async_return

pytestmark = mark.asyncio

@fixture(scope="function")
def token_neuralactions():
    return TokenNeuralAction(
                access_token='access_token_example',
                token_type='Bearer',
                user_id=1,
                expires_at=datetime.now())

@mark.unit
async def test_when_get_token_successfull(token_neuralactions):
    token_repository = Mock()
    neuralactions_repository = Mock()
    
    token_repository.get_token.return_value = async_return(token_neuralactions)
    token_repository.is_expired.return_value = async_return(False)

    neuralactions_domain_service = NeuralactionDomainService(token_repository, neuralactions_repository)

    result = await neuralactions_domain_service.get_token()

    assert result is not None
    assert result == token_neuralactions.access_token


@mark.unit
async def test_when_get_token_and_not_exist_successfull(token_neuralactions):
    token_repository = Mock()
    neuralactions_repository = Mock()
    
    token_repository.get_token.return_value = async_return(None)
    token_repository.is_expired.return_value = async_return(False)
    neuralactions_repository.authenticate.return_value = async_return(token_neuralactions)
    token_repository.save_token.return_value = async_return(True)

    neuralactions_domain_service = NeuralactionDomainService(token_repository, neuralactions_repository)

    result = await neuralactions_domain_service.get_token()

    assert result is not None
    assert result == token_neuralactions.access_token


@mark.unit
async def test_when_get_token_and_is_expire_successfull(token_neuralactions):
    token_repository = Mock()
    neuralactions_repository = Mock()
    
    token_repository.get_token.return_value = async_return(token_neuralactions)
    token_repository.is_expired.return_value = async_return(True)
    token_repository.save_token.return_value = async_return(True)
    neuralactions_repository.authenticate.return_value = async_return(token_neuralactions)

    neuralactions_domain_service = NeuralactionDomainService(token_repository, neuralactions_repository)

    result = await neuralactions_domain_service.get_token()

    assert result is not None
    assert result == token_neuralactions.access_token