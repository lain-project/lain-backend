from unittest.mock import Mock
from app.application.exception.business_error_exception import BusinessErrorException
from app.application.service.security_domain_service import SecurityDomainService
from app.domain.rol import Rol
from app.domain.user import User
from app.domain.token import Token
from pytest import fixture, raises, mark

@fixture(scope="function")
def user():
    return User(
        username="himura",
        password="kenji",
        email="himura@gmail.com",
        avatar="img",
        rol= Rol(1, 'ADMIN', 'Administrador'),
        google=True)



@mark.unit
def test_write_token_is_successfull(user):

    security_repo_implementation = Mock()
    security_repo_implementation.encode.return_value = 'token_example'

    token = Token(access_token='token_example')

    service = SecurityDomainService(security_repo_implementation)

    result = service.write_token(user) 

    assert result == token


@mark.unit
def test_write_token_failure(user):

    security_repo_implementation = Mock()
    message = "Error al decodificar token"
    security_repo_implementation.encode.side_effect= BusinessErrorException(message)

    service = SecurityDomainService(security_repo_implementation)

    with raises(BusinessErrorException, match=message):
        assert service.write_token(user)


@mark.unit
def test_verify_password_successfull(user):

    security_repo_implementation = Mock()
    security_repo_implementation.verify_password.return_value = True

    service = SecurityDomainService(security_repo_implementation)

    result = service.verify_password(user, 'kenji')

    assert result == True


@mark.unit
def test_verify_password_failure(user):

    security_repo_implementation = Mock()
    security_repo_implementation.verify_password.return_value = False

    service = SecurityDomainService(security_repo_implementation)

    with raises(BusinessErrorException, match='Error al validar el password'):
        assert service.verify_password(user, 'kenji')


@mark.unit
def test_generate_hash_password_successfull():

    security_repo_implementation = Mock()
    security_repo_implementation.get_hashed_password.return_value = '$password'

    service = SecurityDomainService(security_repo_implementation)

    result = service.get_password_hash('password')

    assert result == '$password'