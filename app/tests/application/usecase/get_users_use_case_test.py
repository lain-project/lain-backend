from app.application.usecase.get_users_use_case import GetUsersUseCase
from pytest import fixture, mark
from unittest.mock import Mock

from app.domain.rol import Rol
from app.domain.profile import Profile
from app.domain.user import User
from app.domain.enums.profile_position_enum import ProfilePositionEnum
from app.domain.enums.rol_name_enum import RolNameEnum
from app.tests.utils.coroutines import async_return

@fixture(scope="function")
def users():
    admin = Rol(1, RolNameEnum.ADMIN.value, True)
    docente = Rol(2, RolNameEnum.DOCENTE.value, True)

    profile_1 = Profile(firstname="Himura", lastname="Kenji")
    profile_2 = Profile(firstname="Lilith", lastname="Queen", position=ProfilePositionEnum.TITULAR.value)
    profile_3 = Profile(firstname="0x1e", lastname="0x1e", position=ProfilePositionEnum.ADJUNTO.value)

    user_1 = User(username="himura", password="kenji", email="himura@kenji", rol=admin, profile=profile_1)
    user_2 = User(username="lilith", password="lilith", email="lilith@queen", rol=docente , profile=profile_2)
    user_3 = User(username="0x1e", password="0x1e", email="0x1e@0x1e", rol=docente, profile=profile_3)

    return [user_1, user_2, user_3]


@mark.unit
@mark.asyncio
async def test_when_get_users_is_successfull(users):
    user_repository = Mock()
    user_repository.get_all.return_value = async_return(users)

    use_case = GetUsersUseCase(user_repository)
    result = await use_case.execute()

    assert result is not None
    assert len(result) == 3
    assert result[0].username == users[0].username
    assert result[0].password == users[0].password
    assert result[0].email == users[0].email
    assert result[0].rol == users[0].rol
    assert result[0].profile == users[0].profile
    assert result[1].username == users[1].username
    assert result[1].password == users[1].password
    assert result[1].email == users[1].email
    assert result[1].rol == users[1].rol
    assert result[1].profile == users[1].profile
    assert result[2].username == users[2].username
    assert result[2].password == users[2].password
    assert result[2].email == users[2].email
    assert result[2].rol == users[2].rol
    assert result[2].profile == users[2].profile
