from pytest import fixture, raises, mark
from unittest.mock import Mock

from app.adapter.controller.auth_controller_adapter import google
from app.application.exception.bad_request_business_exception import BadRequestBusinessErrorException
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
from app.application.port.output import google_repository
from app.application.usecase.google_login_use_case import GoogleLoginUseCase
from app.domain.user import User
from app.domain.token import Token
from app.tests.utils.coroutines import async_return

@fixture(scope="function")
def user():
    return User(
        username="himura",
        password="kenji",
        email="himura@gmail.com",
    )


@mark.unit
@mark.asyncio
async def test_when_authenticate_is_successfull(user):
    google_repository= Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    google_repository.google_verify.return_value = 'himura@gmail.com'
    user_repository.find_by_email.return_value = async_return(user)
    security_domain_service.write_token.return_value = Token(access_token='access_token_example')
    neuralactions_domain_service.get_token.return_value = async_return('token_neuralaction_example')

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)
    result = await use_case.execute('token_id')

    assert result is not None
    assert result.access_token == 'access_token_example'
    assert result.token_neuralaction == 'token_neuralaction_example'


@mark.unit
@mark.asyncio
async def test_when_user_with_email_not_exist_failure():

    google_repository= Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    message = 'El usuario con email no existe'
    google_repository.google_verify.return_value = 'himura1@gmail.com'
    user_repository.find_by_email.side_effect = NotFoundBusinessErrorException(message)

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)

    with raises(NotFoundBusinessErrorException, match=message):
        assert await use_case.execute('token_id')


@mark.unit
@mark.asyncio
async def test_when_have_problem_in_database_failure():

    google_repository = Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    message = 'El usuario con email no existe'
    google_repository.google_verify.return_value = 'himura1@gmail.com'
    user_repository.find_by_email.side_effect = InternalBusinessErrorException(message)

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)

    with raises(InternalBusinessErrorException, match=message):
        assert await use_case.execute('token_id')


@mark.unit
@mark.asyncio
async def test_when_fail_to_write_token_failure():

    google_repository= Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    message = 'Error al generar el token'
    google_repository.google_verify.return_value = 'himura@gmail.com'
    user_repository.find_by_email.return_value = async_return(user)
    security_domain_service.write_token.side_effect = InternalBusinessErrorException(message)

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)

    with raises(InternalBusinessErrorException, match=message):
        assert await use_case.execute('token_id')


@mark.unit
@mark.asyncio
async def test_when_google_verify_failure():

    google_repository= Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    message = 'El token no es valido'
    google_repository.google_verify.side_effect= BadRequestBusinessErrorException(message)

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)

    with raises(BadRequestBusinessErrorException, match=message):
        assert await use_case.execute('token_id')

@mark.unit
@mark.asyncio
async def test_when_user_not_exist_failure():

    google_repository= Mock()
    user_repository = Mock()
    security_domain_service = Mock()
    neuralactions_domain_service = Mock()

    google_repository.google_verify.return_value = 'himura@gmail.com'
    user_repository.find_by_email.return_value = async_return(None)

    use_case = GoogleLoginUseCase(google_repository, user_repository, security_domain_service, neuralactions_domain_service)

    message = 'El usuario con email himura@gmail.com no existe'

    with raises(NotFoundBusinessErrorException, match=message):
        assert await use_case.execute('token_id')
