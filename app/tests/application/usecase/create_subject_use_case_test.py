from unittest.mock import Mock

from app.tests.utils.coroutines import async_return

from app.domain.year import Year

from app.domain.study_plan import StudyPlan

from app.domain.materia import Materia
from app.application.usecase.create_subject_use_case import CreateSubjectUseCase
from pytest import mark, fixture

pytestmark = mark.asyncio

@fixture(scope="function")
def subject():
    return Materia(
        id=1,
        image="image",
        name="name",
        code="code",
        regime="regime",
        weekly_hours=1,
        total_hours=1,
        study_plan_id=1,
        year_id=1,
        study_plan_year_id=1
    )

@fixture(scope="function")
def study_plan():
    return StudyPlan(
        id=1,
        code="code",
        name="name",
        description="description",
        years=5,
        workspace_id="workspace_id"
    )

@fixture(scope="function")
def year():
    return Year(
        id=1,
        name="name",
        numeration=5,
        schema_id="schema_id",
        workspace_id="workspace_id",
        node_id="node_id"
    )


@mark.unit
async def test_when_create_subject_is_successfull(subject, study_plan, year):
    subject_repository = Mock()
    study_plan_repository = Mock()
    year_repository = Mock()
    neuralactions_repository = Mock()

    subject_repository.find_by_code.return_value = async_return(None)
    subject_repository.save.return_value = async_return(subject)
    study_plan_repository.find_by_id.return_value = async_return(study_plan)
    year_repository.find_by_id.return_value = async_return(year)
    year_repository.find_by_id_and_study_plan_id.return_value = async_return(year)
    neuralactions_repository.save_subject.return_value = async_return(subject)

    create_subject_use_case = CreateSubjectUseCase(subject_repository
                                                   ,study_plan_repository
                                                   ,year_repository
                                                   ,neuralactions_repository)

    subject_created = await create_subject_use_case.execute(subject)

    assert subject_created == subject
