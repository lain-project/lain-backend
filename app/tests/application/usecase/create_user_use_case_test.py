from unittest.mock import Mock
from app.application.exception.conflict_business_exception import ConflictBusinessException
from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.usecase.create_user_use_case import CreateUserUseCase
from app.domain.enums.profile_position_enum import ProfilePositionEnum
from pytest import fixture, mark, raises
from app.domain.profile import Profile
from app.domain.user import User
from app.tests.utils.coroutines import async_return



@fixture(scope="function")
def user():
    profile = Profile(
        firstname="Himura",
        lastname="Kenji",
        position=ProfilePositionEnum.TITULAR.value
    )
    return User(
        username="himura",
        password="kenji",
        email="himura@gmail.com",
        profile=profile
    )

@mark.unit
@mark.asyncio
async def test_when_create_user_is_successfull(user):
    user_repository = Mock()
    security_domain_service = Mock()

    user_repository.find_by_username.return_value = async_return(None)
    user_repository.find_by_email.return_value = async_return(None)
    user_repository.create.return_value = async_return(user)

    security_domain_service.get_password_hash.return_value = '$password_hash'

    use_case = CreateUserUseCase(user_repository, security_domain_service)
    result = await use_case.execute(user)

    assert result is not None
    assert result.username == user.username
    assert result.password == '$password_hash'
    assert result.email == user.email
    assert result.profile == user.profile

@mark.unit
@mark.asyncio
async def test_when_create_user_exist_by_username_failure(user):
    user_repository = Mock()
    security_domain_service = Mock()
    message = "Username already exists"

    user_repository.find_by_username.return_value = async_return(user)

    use_case = CreateUserUseCase(user_repository, security_domain_service)
    with raises(ConflictBusinessException, match=message):
        assert await use_case.execute(user)

@mark.unit
@mark.asyncio
async def test_when_create_user_exist_by_email_failure(user):
    user_repository = Mock()
    security_domain_service = Mock()
    message = "Email already exists"

    user_repository.find_by_username.return_value = async_return(None)
    user_repository.find_by_email.return_value = async_return(user)


    use_case = CreateUserUseCase(user_repository, security_domain_service)
    with raises(ConflictBusinessException, match=message):
        assert await use_case.execute(user)


@mark.unit
@mark.asyncio
async def test_when_create_user_and_save_failure(user):
    user_repository = Mock()
    security_domain_service = Mock()
    message = 'Error en la base de datos'

    user_repository.find_by_username.return_value = async_return(None)
    user_repository.find_by_email.return_value = async_return(None)
    user_repository.create.side_effect = InternalBusinessErrorException(message)

    security_domain_service.get_password_hash.return_value = '$password_hash'

    use_case = CreateUserUseCase(user_repository, security_domain_service)
    with raises(InternalBusinessErrorException, match=message):
        assert await use_case.execute(user)
