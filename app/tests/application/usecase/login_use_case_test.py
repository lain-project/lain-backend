from pytest import fixture, raises,mark
from unittest.mock import Mock 

from app.application.exception.internal_bussiness_error_exception import InternalBusinessErrorException
from app.application.exception.not_found_business_error_exception import NotFoundBusinessErrorException
from app.application.exception.unauthorized_error_exception import UnauthorizedErrorException
from app.domain.token import Token
from app.domain.user import User
from app.application.usecase.login_use_case import LoginUseCase
from app.tests.utils.coroutines import async_return

@fixture(scope="function")
def user():
    return User(
        username="himura",
        password="kenji",
        email="himura@kenji",
    )


@mark.unit
@mark.asyncio
async def test_when_authenticate_is_successfull(user):
    
    user_repository = Mock()
    neuralactions_domain_service = Mock()
    security_domain_service = Mock()

    user_repository.find_by_username.return_value =  async_return(user)
    security_domain_service.verify_password.return_value = True
    security_domain_service.write_token.return_value = Token(access_token='access_token_example')
    neuralactions_domain_service.get_token.return_value = async_return('token_neuralaction_example')


    use_case = LoginUseCase(user_repository, security_domain_service, neuralactions_domain_service)

    username = 'himura'
    password = 'kenji'

    result = await use_case.execute(username, password)

    assert result is not None
    assert result.access_token == 'access_token_example'
    assert result.token_neuralaction == 'token_neuralaction_example'


@mark.unit
@mark.asyncio
async def test_when_username_not_exist_failure():

    user_repository = Mock()
    neuralactions_domain_service = Mock()
    security_domain_service = Mock()
    message = 'El usuario username no existe'
    user_repository.find_by_username.return_value = async_return(None)

    use_case = LoginUseCase(user_repository, security_domain_service, neuralactions_domain_service)

    with raises(NotFoundBusinessErrorException, match=message):
        assert await use_case.execute('username', 'password')


@mark.unit
@mark.asyncio
async def test_when_password_is_invalid_failure(user):

    user_repository = Mock()
    neuralactions_domain_service = Mock()
    security_domain_service = Mock()

    user_repository.find_by_username.return_value = async_return(user)
    message = 'Error al validar el password'
    security_domain_service.verify_password.side_effect = UnauthorizedErrorException(message)

    use_case = LoginUseCase(user_repository, security_domain_service, neuralactions_domain_service)

    with raises(UnauthorizedErrorException, match=message) as error: 
        assert await use_case.execute('username', 'password')

 
@mark.unit
@mark.asyncio
async def test_when_fail_to_write_token_failure(user):

    user_repository = Mock()
    neuralactions_domain_service = Mock()
    security_domain_service = Mock()

    message = 'Error al generar el token'
    user_repository.find_by_username.return_value = async_return(user)
    security_domain_service.verify_password.return_value = True
    security_domain_service.write_token.side_effect = InternalBusinessErrorException(message)

    use_case = LoginUseCase(user_repository, security_domain_service, neuralactions_domain_service)

    with raises(InternalBusinessErrorException, match=message):
        assert await use_case.execute('username', 'password')


