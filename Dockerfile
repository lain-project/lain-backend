FROM python:3.10

RUN mkdir lain-back

WORKDIR /lain-back

COPY . .

RUN pip install poetry 

RUN poetry config virtualenvs.create false
RUN poetry install -n --no-ansi